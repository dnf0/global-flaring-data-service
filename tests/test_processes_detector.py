import os
import shutil
from unittest import mock
from pathlib import Path
from collections import namedtuple

import pytest
import pandas as pd
from numpy.testing import assert_allclose

import service_constants
import processes.utils as utils
from processes.detector import SLSHotspotDetector, SLSSamplingDetector


TEST_DIR = os.path.dirname(os.path.realpath(__file__))
TEST_DATA_DIR = os.path.join(TEST_DIR, 'test_files')

# Expected outputs
DF_PATH = os.path.join(TEST_DATA_DIR, "dataframes")
TARGET_HOTSPOT_DF = pd.read_csv(os.path.join(DF_PATH, "hotspots.csv"), index_col=[0])
TARGET_SAMPLING_DF = pd.read_csv(os.path.join(DF_PATH, "sampling.csv"), index_col=[0])
TARGET_PERSISTENT_DF = pd.read_csv(os.path.join(DF_PATH, "persistent.csv"), index_col=[0])


# https://adamj.eu/tech/2020/10/13/how-to-mock-environment-variables-with-pytest/
@pytest.fixture(autouse=True)
def mock_settings_env_vars():
    with mock.patch.dict(os.environ,
                         {"OUTPUT_ROOT": os.path.join(TEST_DATA_DIR, "output"),
                          "BATCH_ROOT": os.path.join(TEST_DATA_DIR, "batch"),
                          "UNZIP_ROOT": os.path.join(TEST_DATA_DIR, "unzipped"),
                          "SAT_DATA_ROOT": os.path.join(TEST_DATA_DIR, "slstr_filetree/neodc"),
                          "DB_DIR": TEST_DATA_DIR,
                          "DB_NAME": "test.db"}):
        yield


def test_env():
    assert os.environ["OUTPUT_ROOT"] == os.path.join(TEST_DATA_DIR, "output")
    assert os.environ["BATCH_ROOT"] == os.path.join(TEST_DATA_DIR, "batch")
    assert os.environ["UNZIP_ROOT"] == os.path.join(TEST_DATA_DIR, "unzipped")
    assert os.environ["SAT_DATA_ROOT"] == os.path.join(TEST_DATA_DIR, "slstr_filetree/neodc")


def test_directory_setup():
    # create output directory
    root = [os.environ.get("OUTPUT_ROOT"), os.environ.get("BATCH_ROOT"), os.environ.get("UNZIP_ROOT")]
    for r in root:
        if not os.path.exists(r):
            os.makedirs(r)


@pytest.fixture
def test_file_details():
    for test_file in Path(os.environ["SAT_DATA_ROOT"]).rglob('*.zip'):
        break
    split = str(test_file).split('/')
    path, file = "/".join(split[:-1]), split[-1]
    file_details = namedtuple("file_details", "id path file hotspot_populated")
    return file_details(1, path, file, 0)


def test_hotspot_detector(test_file_details):
    input_path = os.path.join(test_file_details.path, test_file_details.file)
    product = utils.unzipper(input_path, os.environ.get('UNZIP_ROOT'))

    hotspot_detector = SLSHotspotDetector(product, 'S3A')
    hotspot_detector.run_detector()
    keys = ['latitude', 'longitude', 'frp', 'sza', 'vza', 'swir_16', 'swir_22', 'pixel_size']
    df = hotspot_detector.to_dataframe(keys=keys)
    assert_allclose(df[keys].values, TARGET_HOTSPOT_DF[keys].values)

    output_path = utils.define_batch_process_output_path(test_file_details.path,
                                                         os.environ.get("OUTPUT_ROOT"))
    output_fname = test_file_details.file.replace(".zip", service_constants.HOTSPOT_SUFFIX)
    df.to_csv(os.path.join(output_path, output_fname))


def test_sampling_detector(test_file_details):
    input_path = os.path.join(test_file_details.path, test_file_details.file)
    product = utils.unzipper(input_path, os.environ.get('UNZIP_ROOT'))

    sampling_detector = SLSSamplingDetector(product, "S3A")
    sampling_detector.run_detector()

    keys = ['latitude', 'longitude', 'local_cloudiness']

    df = sampling_detector.to_dataframe(keys=keys, joining_df=TARGET_PERSISTENT_DF, sampling=True)
    assert_allclose(df[keys].values, TARGET_SAMPLING_DF[keys].values)

    output_path = utils.define_batch_process_output_path(test_file_details.path,
                                                         os.environ.get("OUTPUT_ROOT"))
    output_fname = test_file_details.file.replace(".zip", service_constants.SAMPLING_SUFFIX)
    df.to_csv(os.path.join(output_path, output_fname))


def test_directory_teardown():
    if os.path.exists(os.path.join(TEST_DATA_DIR, "test.db")):
        os.remove(os.path.join(TEST_DATA_DIR, "test.db"))
    shutil.rmtree(os.path.join(os.environ.get("OUTPUT_ROOT")))
    shutil.rmtree(os.path.join(os.environ.get("BATCH_ROOT")))
    shutil.rmtree(os.path.join(os.environ.get("UNZIP_ROOT")))
