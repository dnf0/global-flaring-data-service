import os
import shutil
import threading
import pytest
import time
import pickle
from pathlib import Path
from unittest import mock
from collections import namedtuple

import pandas as pd
from numpy.testing import assert_allclose

import service_constants
import processes.threads as threads
import processes.utils as utils
from service_constants import TASK_QUEUES
from processes.data import BatchItem, PrioritizedTask
import processes.tasks as tasks

TEST_DIR = os.path.dirname(os.path.realpath(__file__))
TEST_DATA_DIR = os.path.join(TEST_DIR, 'test_files')

# Expected outputs
DF_PATH = os.path.join(TEST_DATA_DIR, "dataframes")
TARGET_HOTSPOT_DF = pd.read_csv(os.path.join(DF_PATH, "hotspots.csv"), index_col=[0])
TARGET_SAMPLING_DF = pd.read_csv(os.path.join(DF_PATH, "sampling.csv"), index_col=[0])
TARGET_PERSISTENT_DF = pd.read_csv(os.path.join(DF_PATH, "persistent.csv"), index_col=[0])


# https://adamj.eu/tech/2020/10/13/how-to-mock-environment-variables-with-pytest/
@pytest.fixture(autouse=True)
def mock_settings_env_vars():
    with mock.patch.dict(os.environ,
                         {"OUTPUT_ROOT": os.path.join(TEST_DATA_DIR, "output"),
                          "BATCH_ROOT": os.path.join(TEST_DATA_DIR, "batch"),
                          "UNZIP_ROOT": os.path.join(TEST_DATA_DIR, "unzipped"),
                          "SAT_DATA_ROOT": os.path.join(TEST_DATA_DIR, "slstr_filetree/neodc"),
                          "DB_DIR": TEST_DATA_DIR,
                          "DB_NAME": "test.db"}):
        yield


def test_env():
    assert os.environ["OUTPUT_ROOT"] == os.path.join(TEST_DATA_DIR, "output")
    assert os.environ["BATCH_ROOT"] == os.path.join(TEST_DATA_DIR, "batch")
    assert os.environ["UNZIP_ROOT"] == os.path.join(TEST_DATA_DIR, "unzipped")
    assert os.environ["SAT_DATA_ROOT"] == os.path.join(TEST_DATA_DIR, "slstr_filetree/neodc")


def test_directory_setup():
    # create output directory
    root = [os.environ.get("OUTPUT_ROOT"), os.environ.get("BATCH_ROOT"), os.environ.get("UNZIP_ROOT")]
    for r in root:
        if not os.path.exists(r):
            os.makedirs(r)


@pytest.fixture
def test_file_details():
    for test_file in Path(os.environ["SAT_DATA_ROOT"]).rglob('*.zip'):
        break
    split = str(test_file).split('/')
    path, file = "/".join(split[:-1]), split[-1]
    file_details = namedtuple("file_details", "id path file hotspot_populated")
    return file_details(1, path, file, 0)


def test_add_select_seen_task_to_db_queue():
    thread = threading.Thread(target=threads.add_select_seen_task_to_db_queue, daemon=True)
    thread.start()
    task = TASK_QUEUES['db'].get()
    assert task.priority == service_constants.DUMP_SEEN_PRIORITY
    assert task.data is None
    assert task.f == tasks.select_seen
    assert TASK_QUEUES['db'].empty()


def test_add_insert_unseen_files_task_to_db_queue():
    # create an empty seen file
    with open(os.path.join(os.environ.get('OUTPUT_ROOT'), "seen.pickle"), 'wb') as f:
        pickle.dump(set(), f)

    TASK_QUEUES['update_unseen'].put(True)  # only runs if an update command issued
    thread = threading.Thread(target=threads.add_insert_unseen_files_task_to_db_queue, daemon=True)
    thread.start()
    task = TASK_QUEUES['db'].get()
    assert task.priority == service_constants.INSERT_UNSEEN_PRIORITY
    assert task.data is None
    assert task.f == tasks.insert_unseen
    assert TASK_QUEUES['db'].empty()


def test_add_select_files_for_batch_proc_task_to_db_queue():
    TASK_QUEUES['select_batch'].put(True)
    thread = threading.Thread(target=threads.add_select_files_for_batch_proc_task_to_db_queue, daemon=True)
    thread.start()
    task = TASK_QUEUES['db'].get()
    assert task.priority == service_constants.SELECT_FOR_BATCH_PRIORITY
    assert task.data is None
    assert task.f == tasks.select_batch
    assert TASK_QUEUES['db'].empty()


def test_submit_batch_process(test_file_details):
    mock_batch_item = BatchItem(id=0,
                                path=test_file_details.path,
                                file=test_file_details.file,
                                populated=0,
                                attempts=0,
                                last_sampled_unix_time=0)

    if os.path.exists(os.path.join(os.environ.get("OUTPUT_ROOT"), '2020')):
        shutil.rmtree(os.path.join(os.environ.get("OUTPUT_ROOT"), '2020'))

    TARGET_PERSISTENT_DF.to_csv(os.path.join(os.environ.get("OUTPUT_ROOT"), "persistent.csv"))
    TASK_QUEUES['batch'].put([mock_batch_item])
    thread = threading.Thread(target=threads.submit_batch_process, daemon=True)
    thread.start()

    outpath = utils.define_batch_process_output_path(test_file_details.path, os.environ.get("OUTPUT_ROOT"))
    job_start = time.time()  # give five minutes for job to complete
    while not ((os.path.isfile(
            os.path.join(outpath, test_file_details.file.replace(".zip", service_constants.HOTSPOT_SUFFIX))) and
                os.path.isfile(
                    os.path.join(outpath, test_file_details.file.replace(".zip", service_constants.SAMPLING_SUFFIX))))
               and (time.time() - job_start < 5 * 60)
    ):
        pass

    if time.time() - job_start > 5 * 60:
        raise

    # ensure dataframes are populated
    time.sleep(10)

    keys = ['latitude', 'longitude', 'frp', 'sza', 'vza', 'swir_16', 'swir_22', 'pixel_size']
    hotspot_df = pd.read_csv(os.path.join(outpath,
                                          test_file_details.file.replace(".zip", service_constants.HOTSPOT_SUFFIX)),
                             index_col=[0])
    assert_allclose(hotspot_df[keys].values, TARGET_HOTSPOT_DF[keys].values)

    keys = ['latitude', 'longitude', 'local_cloudiness']
    sampling_df = pd.read_csv(os.path.join(outpath,
                                           test_file_details.file.replace(".zip", service_constants.SAMPLING_SUFFIX)),
                              index_col=[0])
    assert_allclose(sampling_df[keys].values, TARGET_SAMPLING_DF[keys].values)

    assert TASK_QUEUES['db'].qsize() == 1  # update_hotspot_attempt_unix_time jobs expected
    task = TASK_QUEUES['db'].get()
    assert task.f == tasks.update_attempt

    assert TASK_QUEUES['update_hotspot'].qsize() == 1
    task = TASK_QUEUES['update_hotspot'].get()
    assert type(task[0]) == BatchItem

    assert TASK_QUEUES['update_sample'].qsize() == 1
    task = TASK_QUEUES['update_sample'].get()
    assert type(task[0]) == BatchItem


def test_add_insert_hotspot_data_task_to_db_queue(test_file_details):

    mock_job = BatchItem(id=1,
                         path=test_file_details.path,
                         file=test_file_details.file,
                         populated=0,
                         attempts=0,
                         last_sampled_unix_time=0)

    TASK_QUEUES['update_hotspot'].put([mock_job])
    thread = threading.Thread(target=threads.add_insert_hotspot_data_task_to_db_queue, daemon=True)
    thread.start()
    time.sleep(10)  # give chance for thread to run

    total_tasks = 0
    while TASK_QUEUES['db'].qsize():
        task = TASK_QUEUES['db'].get()
        assert (task.priority == service_constants.INSERT_DATA_PRIORITY or
                task.priority == service_constants.UPDATE_POPULATION_PRIORITY)
        if task.priority == service_constants.INSERT_DATA_PRIORITY:
            assert type(task.data) == BatchItem
            assert task.f == tasks.insert_hotspot_data
        else:
            assert type(task.data[0]) == BatchItem
            assert task.f == tasks.update_population
        total_tasks += 1
    assert total_tasks == 2
    assert TASK_QUEUES['db'].empty()


def test_add_insert_sampling_data_task_to_db_queue(test_file_details):

    mock_job = BatchItem(id=1,
                         path=test_file_details.path,
                         file=test_file_details.file,
                         populated=0,
                         attempts=0,
                         last_sampled_unix_time=0)

    TASK_QUEUES['update_sample'].put([mock_job])
    thread = threading.Thread(target=threads.add_insert_sampling_data_task_to_db_queue, daemon=True)
    thread.start()
    time.sleep(10)  # give chance for for thread to run

    total_tasks = 0
    while TASK_QUEUES['db'].qsize():
        task = TASK_QUEUES['db'].get()
        assert (task.priority == service_constants.INSERT_DATA_PRIORITY or
                task.priority == service_constants.UPDATE_SAMPLING_UNIX_TIME_PRIORITY)
        if task.priority == service_constants.INSERT_DATA_PRIORITY:
            assert type(task.data) == BatchItem
            assert task.f == tasks.insert_sample_data
        else:
            assert type(task.data[0]) == BatchItem
            assert task.f == tasks.update_sampling
        total_tasks += 1
    assert total_tasks == 2
    assert TASK_QUEUES['db'].empty()


def test_db_queue():
    while not TASK_QUEUES['db'].empty():
        TASK_QUEUES['db'].get()
    assert TASK_QUEUES['db'].empty()

    # generate some tasks
    select_task = PrioritizedTask(service_constants.SELECT_FOR_BATCH_PRIORITY,
                                  f=None)

    update_task = PrioritizedTask(service_constants.UPDATE_ATTEMPT_PRIORITY,
                                  f=None)

    persist_task = PrioritizedTask(service_constants.DUMP_PERSISTENT_PRIORITY,
                                   f=None)

    [TASK_QUEUES['db'].put(t) for t in [select_task, update_task, persist_task]]

    priorities = [service_constants.SELECT_FOR_BATCH_PRIORITY,
                  service_constants.UPDATE_ATTEMPT_PRIORITY,
                  service_constants.DUMP_PERSISTENT_PRIORITY]
    for p in sorted(priorities):
        assert TASK_QUEUES['db'].get().priority == p
    assert TASK_QUEUES['db'].empty()


@mock.patch('service_constants.PERSISTENT_PERIOD', 0)
def test_add_persistent_extraction_to_db_queue():

    # call with daemon to ensure thread dies when function exits.
    thread = threading.Thread(target=threads.add_persistent_extraction_to_db_queue, daemon=True)
    thread.start()

    # sleep to give change for db queue to finish populating
    time.sleep(1)
    task = TASK_QUEUES['db'].get()
    assert task.priority == service_constants.DUMP_PERSISTENT_PRIORITY
    assert TASK_QUEUES['db'].empty()


def test_directory_teardown():
    if os.path.exists(os.path.join(TEST_DATA_DIR, "test.db")):
        os.remove(os.path.join(TEST_DATA_DIR, "test.db"))
    shutil.rmtree(os.path.join(os.environ.get("OUTPUT_ROOT")))
    shutil.rmtree(os.path.join(os.environ.get("BATCH_ROOT")))
    shutil.rmtree(os.path.join(os.environ.get("UNZIP_ROOT")))
