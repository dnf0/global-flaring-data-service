import os
import shutil
import random
import pickle
from pathlib import Path
from unittest import mock
from collections import namedtuple

import pytest
from sqlalchemy.orm import Session
from sqlalchemy import create_engine

import db.build as build_db
from processes import utils
from processes import data
from db import statements

TEST_DIR = os.path.dirname(os.path.realpath(__file__))
TEST_DATA_DIR = os.path.join(TEST_DIR, 'test_files')


# https://adamj.eu/tech/2020/10/13/how-to-mock-environment-variables-with-pytest/
@pytest.fixture(autouse=True)
def mock_settings_env_vars():
    with mock.patch.dict(os.environ,
                         {"OUTPUT_ROOT": os.path.join(TEST_DATA_DIR, "output"),
                          "BATCH_ROOT": os.path.join(TEST_DATA_DIR, "batch"),
                          "UNZIP_ROOT": os.path.join(TEST_DATA_DIR, "unzipped"),
                          "SAT_DATA_ROOT": os.path.join(TEST_DATA_DIR, "slstr_filetree/neodc"),
                          "MOCK_SAT_DATA_ROOT": os.path.join(TEST_DATA_DIR, "mock_slstr_filetree"),
                          "DB_DIR": TEST_DATA_DIR,
                          "DB_NAME": "test.db"}):
        yield


@pytest.fixture
def slstr_filepath_filename():
    # generate psuedo SLSTR filename
    file = "S3A_SL_1_RBT____202110{}T{}{}{}_LN2_O_NT_004.zip"
    path = "sentinel3a/data/SLSTR/L1_RBT/2021/10/{}"
    return path, file


@pytest.fixture
def test_file_details():
    for test_file in Path(os.environ["SAT_DATA_ROOT"]).rglob('*.zip'):
        break
    split = str(test_file).split('/')
    path, file = "/".join(split[:-1]), split[-1]
    file_details = namedtuple("file_details", "id path file hotspot_populated")
    return file_details(1, path, file, 0)


@pytest.fixture
def test_engine():
    db_path = os.path.join(os.environ.get('DB_DIR'), os.environ.get('DB_NAME'))
    return create_engine(f"sqlite+pysqlite:///{db_path}", echo=False, future=True)


def test_db_build(test_engine):
    build_db.build(test_engine)
    assert os.path.isfile(os.path.join(TEST_DATA_DIR, "test.db"))


def test_env():
    assert os.environ["OUTPUT_ROOT"] == os.path.join(TEST_DATA_DIR, "output")
    assert os.environ["BATCH_ROOT"] == os.path.join(TEST_DATA_DIR, "batch")
    assert os.environ["UNZIP_ROOT"] == os.path.join(TEST_DATA_DIR, "unzipped")
    assert os.environ["SAT_DATA_ROOT"] == os.path.join(TEST_DATA_DIR, "slstr_filetree/neodc")
    assert os.environ["MOCK_SAT_DATA_ROOT"] == os.path.join(TEST_DATA_DIR, "mock_slstr_filetree")


def test_directory_setup():
    # create output directory
    root = [os.environ.get("OUTPUT_ROOT"),
            os.environ.get("BATCH_ROOT"),
            os.environ.get("UNZIP_ROOT"),
            os.environ.get("MOCK_SAT_DATA_ROOT")]
    for r in root:
        if not os.path.exists(r):
            os.makedirs(r)


def test_batch_split():
    for n_files in [0, 101, 1000, 990, 10000, 100001]:
        test_files = [None] * n_files
        batches, batch_sizes = utils.batch_split(test_files)
        assert sum([len(batch) for batch in batches]) == n_files
        assert sum(batch_sizes) == n_files


def test_define_batch_process_output_path(test_file_details):
    outpath = utils.define_batch_process_output_path(test_file_details.path,
                                                     os.environ.get("OUTPUT_ROOT"))
    assert outpath == os.path.join(os.environ.get("OUTPUT_ROOT"), "2020/02/22")


def test_unzipper(test_file_details):
    input_path = os.path.join(test_file_details.path, test_file_details.file)
    product = utils.unzipper(input_path, os.environ.get('UNZIP_ROOT'))
    for k in ["S5_radiance_an", "S6_radiance_an",
              "geodetic_an", "geometry_tn",
              "cartesian_an", "cartesian_tx",
              "indices_an", "flags_an", "time_an",
              "flags_in"]:
        assert k in product
    remaining_files = os.listdir(os.environ.get('UNZIP_ROOT'))
    if ".DS_Store" in remaining_files:
        remaining_files.remove(".DS_Store")
    assert not remaining_files


def test_fail_find_unseen_files():
    with pytest.raises(FileNotFoundError):
        utils.find_unseen_files()


def test_empty_dump_seen(test_engine):

    with Session(test_engine) as session:
        utils.dump_seen(session, statements.select_seen_stmt())
    assert os.path.isfile(os.path.join(os.environ.get('OUTPUT_ROOT'), "seen.pickle"))

    with open(os.path.join(os.environ.get('OUTPUT_ROOT'), "seen.pickle"), 'rb') as f:
        s = pickle.load(f)
    assert s == set()


def test_find_unseen_files(slstr_filepath_filename,
                           test_engine):

    # change sat data to match mock root for this test
    os.environ["SAT_DATA_ROOT"] = os.environ["MOCK_SAT_DATA_ROOT"]

    # ensure seen is empty
    with open(os.path.join(os.environ.get("OUTPUT_ROOT"), 'seen.pickle'), 'wb') as f:
        pickle.dump(set(), f)

    # iterate over range of days
    for day_of_month in range(10, 30, 2):

        # generate mock data
        test_files = set()
        for _ in range(25):
            path, file = slstr_filepath_filename

            # randomise time stamp
            file = file.format(day_of_month,
                               str(random.randint(1, 23)).zfill(2),
                               str(random.randint(1, 59)).zfill(2),
                               str(random.randint(1, 59)).zfill(2)
                               )

            root = os.path.join(os.environ.get("MOCK_SAT_DATA_ROOT"), path.format(day_of_month))
            if not os.path.exists(root):
                os.makedirs(root)
            with open(os.path.join(root, file), "w") as f:
                f.write("")
            test_files.add(file)

        # assert unseen is equivalent to mocked files
        unseen = utils.find_unseen_files()
        assert set([u.file for u in unseen]) == test_files

        # insert mocked data into database
        unseen = data.wrangle_for_unseen(unseen)
        with Session(test_engine) as session:
            session.execute(statements.bulk_insert_filelist_stmt(), unseen)
            session.commit()

        # dump to seen
        with Session(test_engine) as session:
            utils.dump_seen(session, statements.select_seen_stmt())


def test_directory_teardown():
    if os.path.exists(os.path.join(TEST_DATA_DIR, "test.db")):
        os.remove(os.path.join(TEST_DATA_DIR, "test.db"))
    shutil.rmtree(os.path.join(os.environ.get("OUTPUT_ROOT")))
    shutil.rmtree(os.path.join(os.environ.get("BATCH_ROOT")))
    shutil.rmtree(os.path.join(os.environ.get("UNZIP_ROOT")))
    shutil.rmtree(os.path.join(os.environ.get("MOCK_SAT_DATA_ROOT")))
