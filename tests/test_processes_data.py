import os
import shutil
from collections import namedtuple
from unittest import mock
import pytest

import numpy as np
import pandas as pd
from numpy.testing import assert_allclose

import processes.data as data_processes

TEST_DIR = os.path.dirname(os.path.realpath(__file__))
TEST_DATA_DIR = os.path.join(TEST_DIR, 'test_files')

DF_PATH = os.path.join(TEST_DATA_DIR, "dataframes")
MERGED_DF = pd.read_csv(os.path.join(DF_PATH, "merged.csv"), index_col=0)
TARGET_ACTIVITY_DF = pd.read_json(os.path.join(DF_PATH, "activity.jsonl"), lines=True)


# https://adamj.eu/tech/2020/10/13/how-to-mock-environment-variables-with-pytest/
@pytest.fixture(autouse=True)
def mock_settings_env_vars():
    with mock.patch.dict(os.environ,
                         {"XFC_ROOT": os.path.join(TEST_DATA_DIR, "xfc"),
                          "OUTPUT_ROOT": os.path.join(TEST_DATA_DIR, "output"),
                          "BATCH_ROOT": os.path.join(TEST_DATA_DIR, "batch"),
                          "UNZIP_ROOT": os.path.join(TEST_DATA_DIR, "unzipped"),
                          "SAT_DATA_ROOT": os.path.join(TEST_DATA_DIR, "slstr_filetree/neodc"),
                          "DB_DIR": TEST_DATA_DIR,
                          "DB_NAME": "test.db"}):
        yield


def test_env():
    assert os.environ["XFC_ROOT"] == os.path.join(TEST_DATA_DIR, "xfc")
    assert os.environ["OUTPUT_ROOT"] == os.path.join(TEST_DATA_DIR, "output")
    assert os.environ["BATCH_ROOT"] == os.path.join(TEST_DATA_DIR, "batch")
    assert os.environ["UNZIP_ROOT"] == os.path.join(TEST_DATA_DIR, "unzipped")
    assert os.environ["SAT_DATA_ROOT"] == os.path.join(TEST_DATA_DIR, "slstr_filetree/neodc")


def test_directory_setup():
    # create output directory
    root = [os.environ.get("XFC_ROOT"),
            os.environ.get("OUTPUT_ROOT"),
            os.environ.get("BATCH_ROOT"),
            os.environ.get("UNZIP_ROOT")]
    for r in root:
        if not os.path.exists(r):
            os.makedirs(r)


def test_wrangle_for_batch():
    row = {
        'id': 1,
        'path': "",
        'file': "",
        'populated': 0,
        'attempts': 0,
        'last_sampled_unix_time': 0
    }
    batch_item = data_processes.wrangle_for_batch([row])[0]
    assert all([row[field] == getattr(batch_item, field) for field in row.keys()])


def test_wrangle_for_unseen():
    mock_input = namedtuple("mock_input", "path file")
    mock_input.file = ""
    mock_input.path = ""
    row = data_processes.wrangle_for_unseen([mock_input])[0]
    assert row['file'] == mock_input.file
    assert row['path'] == mock_input.path


def test_wrangle_for_sampling():
    mock_input = namedtuple("mock_input", 'id')
    mock_input.id = 0
    output = data_processes.wrangle_for_sampling([mock_input])[0]
    assert output['id'] == mock_input.id
    assert type(output['last_sampled_unix_time']) == float


def test_wrangle_for_hotspot_attempt():
    mock_input = namedtuple("mock_input", 'id attempts')
    mock_input.id = 0
    mock_input.attempts = 0
    output = data_processes.wrangle_for_hotspot_attempt([mock_input])[0]
    assert output['id'] == mock_input.id
    assert output['attempts'] == mock_input.attempts + 1
    assert type(output['last_process_attempt_unix_time']) == float


def test_wrangle_for_hotspot_population():
    mock_input = namedtuple("mock_input", 'id')
    mock_input.id = 0
    output = data_processes.wrangle_for_hotspot_population([mock_input])[0]
    assert output['id'] == mock_input.id
    assert output['populated'] == 1


def test_upsample_dataframes():
    # generate some fake data
    days, months = zip(*[(str(x), str(y)) for x in range(1, 29) for y in range(1, 13)])

    target_frp = 1
    hotspot_obs = 100
    selector = np.random.choice(len(days), hotspot_obs, replace=False)
    d = {
        "grid_x": np.ones(hotspot_obs).astype(int),
        "grid_y": np.ones(hotspot_obs).astype(int),
        "year": '2020',
        "month": np.array(months)[selector],
        "day": np.array(days)[selector],
        "hour": ['12'] * hotspot_obs,
        "minute": ['30'] * hotspot_obs,
        "frp": np.ones(hotspot_obs) * target_frp,
        "swir_16": np.ones(hotspot_obs),
        "swir_22": np.ones(hotspot_obs),
    }
    mock_hotspot_df = pd.DataFrame(data=d)

    overpasses = 200
    cloud_cover = 0.5
    selector = np.random.choice(len(days), overpasses, replace=False)
    d = {
        "grid_x": np.ones(overpasses).astype(int),
        "grid_y": np.ones(overpasses).astype(int),
        "year": '2020',
        "month": np.array(months)[selector],
        "day": np.array(days)[selector],
        "hour": ['12'] * overpasses,
        "minute": ['30'] * overpasses,
        "local_cloudiness": np.ones(overpasses)*cloud_cover,
    }
    mock_sampling_df = pd.DataFrame(data=d)

    merged = data_processes.upsample_dataframes(mock_hotspot_df, mock_sampling_df)

    # assess the outputs from merged
    assert merged.normalised_frp.median().round() == target_frp  # won't be exactly, but close to


def test_dump_activity_to_lines():
    data_processes.dump_activity_to_lines(MERGED_DF.to_dict(orient='records'))
    activity_df = pd.read_json(os.path.join(os.environ["XFC_ROOT"], "activity.jsonl"), lines=True)
    keys = ['grid_x', 'grid_y', 'normalised_frp', 'expected_cloud_free_overpasses', 'median_swir_ratio']
    assert_allclose(activity_df[keys], TARGET_ACTIVITY_DF[keys])


def test_directory_teardown():
    if os.path.exists(os.path.join(TEST_DATA_DIR, "test.db")):
        os.remove(os.path.join(TEST_DATA_DIR, "test.db"))
    shutil.rmtree(os.path.join(os.environ.get("XFC_ROOT")))
    shutil.rmtree(os.path.join(os.environ.get("OUTPUT_ROOT")))
    shutil.rmtree(os.path.join(os.environ.get("BATCH_ROOT")))
    shutil.rmtree(os.path.join(os.environ.get("UNZIP_ROOT")))
