from sqlalchemy import Column, Integer, String, ForeignKey, Float, UniqueConstraint
from sqlalchemy.orm import declarative_base

Base = declarative_base()


class File(Base):
    __tablename__ = 'filelist'

    id = Column(Integer, primary_key=True)
    file = Column(String(100), unique=True)
    path = Column(String(100))
    time_added = Column(String(30))
    populated = Column(Integer)
    attempts = Column(Integer)
    last_process_attempt_unix_time = Column(Integer)
    last_sampled_unix_time = Column(Integer)


class Hotspot(Base):
    __tablename__ = 'hotspots'

    id = Column(Integer, primary_key=True)
    filelist_id = Column(Integer, ForeignKey('filelist.id'))
    latitude = Column(Float)
    longitude = Column(Float)
    frp = Column(Float)
    sza = Column(Float)
    vza = Column(Float)
    swir_16 = Column(Float)
    swir_22 = Column(Float)
    pixel_size = Column(Float)
    line = Column(Integer)
    sample = Column(Integer)
    grid_x = Column(Integer)
    grid_y = Column(Integer)
    year = Column(Integer)
    month = Column(Integer)
    day = Column(Integer)
    hour = Column(Integer)
    minute = Column(Integer)
    day_of_year = Column(Integer)
    days_since_launch = Column(Integer)
    sensor = Column(String)


class Sample(Base):
    __tablename__ = "sampling"

    id = Column(Integer, primary_key=True)
    filelist_id = Column(Integer, ForeignKey('filelist.id'))
    grid_x = Column(Integer)
    grid_y = Column(Integer)
    latitude = Column(Float)
    longitude = Column(Float)
    local_cloudiness = Column(Float)
    year = Column(Integer)
    month = Column(Integer)
    day = Column(Integer)
    hour = Column(Integer)
    minute = Column(Integer)
    day_of_year = Column(Integer)
    days_since_launch = Column(Integer)
    sensor = Column(String)
    __table_args__ = (UniqueConstraint('grid_x', 'grid_y', 'year', 'month', 'day', 'hour', 'minute'),)


def build(engine) -> None:

    # NOTE create_all checks is table exist before creation, so should not overwrite
    Base.metadata.create_all(engine)
