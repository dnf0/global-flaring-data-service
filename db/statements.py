from sqlalchemy import insert, select, or_, and_
import time

import service_constants
from db.build import File, Sample, Hotspot


def select_seen_stmt() -> select:
    return select(File.path)


# no bulk insert or ignore in ORM
# https://github.com/sqlalchemy/sqlalchemy/issues/5374
def bulk_insert_filelist_stmt() -> insert:
    return insert(File).prefix_with("OR IGNORE")


def bulk_insert_sample_stmt() -> insert:
    return insert(Sample).prefix_with("OR IGNORE")


def bulk_insert_hotspot_stmt() -> insert:
    return insert(Hotspot)


def select_files_to_process_stmt() -> select:
    epoch_time = int(time.time())
    return (select(File.id, File.file, File.path, File.attempts,
                   File.populated, File.last_process_attempt_unix_time, File.last_sampled_unix_time).
            filter(or_(epoch_time - File.last_sampled_unix_time >= service_constants.PERSISTENT_PERIOD,
                       and_(File.populated == 0,
                            File.attempts <= service_constants.MAX_ATTEMPTS,
                            epoch_time - File.last_process_attempt_unix_time >= service_constants.PROC_ATTEMPT_PERIOD)))
            )
