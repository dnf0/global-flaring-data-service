import os
import time
import logging
import threading
from logging.handlers import RotatingFileHandler

from sqlalchemy import create_engine

import db.build as build_db
import processes.threads as thread_processes

from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())


class TerminableThread(threading.Thread):
    """
    Overrides run method of Thread to provide
    support exception monitoring.
    """
    def __init__(self, *args, **kwargs):
        super(TerminableThread, self).__init__(*args, **kwargs)
        self.stop_requested = threading.Event()
        self.exception = None

    # override run method
    def run(self):
        try:
            if self._target:
                self._target(*self._args, **self._kwargs)
        except Exception as e:
            self.exception = e
        finally:
            # Avoid a refcycle if the thread is running a function with
            # an argument that has a member that points to the thread.
            del self._target, self._args, self._kwargs

    def stop(self):
        # set the event to signal stop
        self.stop_requested.set()


def thread_sentinel(threads):
    """
    Monitors threads for exceptions.  If an
    exception is found it is raised and all
    threads stopped.

    :param threads: threads to monitor
    :return:
    """
    try:
        while True:
            for t in threads:
                if t.exception:
                    raise t.exception
            time.sleep(1)

    except Exception as e:
        logging.exception(e)

    finally:
        logging.error("Thread failed, cleaning up other threads.")
        for t in threads:
            t.stop()


def main() -> None:

    log_format = "%(asctime)s %(levelname)s: [%(threadName)s %(filename)s:%(lineno)s - %(funcName)40s() ] %(message)s"
    logging.basicConfig(level=logging.INFO,
                        format=log_format,
                        handlers=[RotatingFileHandler('./service.log', maxBytes=100000, backupCount=10)])

    db_path = os.path.join(os.environ.get('DB_DIR'), os.environ.get('DB_NAME'))
    engine = create_engine(f"sqlite+pysqlite:///{db_path}", echo=False, future=True)
    legacy_engine = create_engine(f"sqlite+pysqlite:///{db_path}", echo=False, future=False)
    build_db.build(engine)

    threads = []
    # setup thread_list
    logging.debug("Setting up thread_list")
    threads.append(TerminableThread(target=thread_processes.add_persistent_extraction_to_db_queue))
    threads.append(TerminableThread(target=thread_processes.db_queue_manager, args=(engine, legacy_engine,)))
    threads.append(TerminableThread(target=thread_processes.add_select_seen_task_to_db_queue))
    threads.append(TerminableThread(target=thread_processes.add_insert_unseen_files_task_to_db_queue))
    threads.append(TerminableThread(target=thread_processes.add_select_files_for_batch_proc_task_to_db_queue))
    threads.append(TerminableThread(target=thread_processes.submit_batch_process))
    threads.append(TerminableThread(target=thread_processes.add_insert_hotspot_data_task_to_db_queue))
    threads.append(TerminableThread(target=thread_processes.add_insert_sampling_data_task_to_db_queue))
    threads.append(TerminableThread(target=thread_processes.add_activity_extraction_to_db_queue))

    logging.debug("Starting and Joining thread_list")
    [t.start() for t in threads]

    thread_sentinel(threads)


if __name__ == "__main__":
    main()
