import os
import logging

import pandas as pd
from sqlalchemy.orm import Session

import db.statements as statements
from db.build import File
import processes.utils as utils
import processes.data as data
from service_constants import TASK_QUEUES
from service_constants import HOTSPOT_SUFFIX
from service_constants import SAMPLING_SUFFIX
from service_constants import JSONL_FNAME


def select_seen(task_args):
    with Session(task_args['engine']) as session:
        utils.dump_seen(session, statements.select_seen_stmt())
        TASK_QUEUES['update_unseen'].put(True)
        TASK_QUEUES['dump_activity'].put(True)
        session.commit()


def insert_unseen(task_args):
    unseen_file_paths = data.wrangle_for_unseen(utils.find_unseen_files())
    logging.info(f"Inserting {len(unseen_file_paths)} new files to db...")
    if unseen_file_paths:
        with Session(task_args['engine']) as session:
            session.execute(statements.bulk_insert_filelist_stmt(), unseen_file_paths)
            session.commit()
    TASK_QUEUES['select_batch'].put(True)


def select_batch(task_args):
    with Session(task_args['engine']) as session:
        rows = session.execute(statements.select_files_to_process_stmt()).all()
    logging.info(f"Submitting {len(rows)} files to batch queue for processing...")
    if rows:
        TASK_QUEUES['batch'].put(data.wrangle_for_batch(rows))


def update_attempt(task_args):
    jobs = task_args['data']
    to_update = data.wrangle_for_hotspot_attempt(jobs)
    logging.info(f"Updating {len(to_update)} attempt and attempt time columns in {File}")
    with Session(task_args['engine']) as session:
        session.bulk_update_mappings(File, to_update)
        session.commit()


def update_population(task_args):
    jobs = task_args['data']
    to_update = data.wrangle_for_hotspot_population(jobs)
    logging.info(f"Updating {len(to_update)} population columns in {File}")
    with Session(task_args['engine']) as session:
        session.bulk_update_mappings(File, to_update)
        session.commit()


def update_sampling(task_args):
    jobs = task_args['data']
    to_update = data.wrangle_for_sampling(jobs)
    logging.info(f"Updating {len(to_update)} sampling time columns in {File}")
    with Session(task_args['engine']) as session:
        session.bulk_update_mappings(File, to_update)
        session.commit()


def insert_hotspot_data(task_args):
    job = task_args['data']
    path = utils.define_batch_process_output_path(job.path, os.environ.get('OUTPUT_ROOT'))
    file = job.file.replace('.zip', HOTSPOT_SUFFIX)
    csv_file_path = os.path.join(path, file)

    df = utils.retry_dataframe_loading(csv_file_path)
    # if dataframe is none then no data was found for processed
    # file.  Eventually, for hotspots processing, the file will
    # stop being processed.  This occurs after MAX_ATTEMPTS in
    # constants (So do not need to reattach job to jobs queue)
    if df is None or df.empty:
        return

    # insert foreign key column and create task for df
    df['filelist_id'] = job.id
    logging.info(f"Inserting {df.shape[0]} hotspot rows into db from {csv_file_path}")
    with Session(task_args['engine']) as session:
        session.execute(statements.bulk_insert_hotspot_stmt(),
                        df.to_dict('records'))
        session.commit()


def insert_sample_data(task_args):
    job = task_args['data']
    path = utils.define_batch_process_output_path(job.path, os.environ.get('OUTPUT_ROOT'))
    file = job.file.replace('.zip', SAMPLING_SUFFIX)
    csv_file_path = os.path.join(path, file)

    df = utils.retry_dataframe_loading(csv_file_path)
    # if dataframe is none then no data was found for processed
    # file. Do not add back to queue, but need to update last sampling time
    # so add to completed jobs for further processing
    if df is None or df.empty:
        return

    # insert foreign key column and create task
    df['filelist_id'] = job.id
    logging.info(f"Inserting {df.shape[0]} sampling rows into db from {csv_file_path}")

    with Session(task_args['engine']) as session:
        session.execute(statements.bulk_insert_sample_stmt(),
                        df.to_dict('records'))
        session.commit()


def dump_persistent(task_args) -> None:
    hotspot_chunks = pd.read_sql_table("hotspots",
                                       task_args['legacy_engine'],
                                       chunksize=50000,
                                       columns=['grid_x', 'grid_y', 'year', 'month', 'day'])
    data.dump_persistent(utils.process_chunks(hotspot_chunks))


def dump_activity(task_args) -> None:
    """
    Resamples and dumps sampling and hostpot data to json

    :param task_args: task arguments
    :return: None
    """

    # remove existing jsonl file
    if os.path.isfile(os.path.join(os.environ.get("XFC_ROOT"), JSONL_FNAME)):
        os.remove(os.path.join(os.environ.get("XFC_ROOT"), JSONL_FNAME))

    persistent_df = pd.read_csv(os.path.join(os.environ.get("OUTPUT_ROOT"), "persistent.csv"), index_col=[0])

    # load entire hotspot and sampling dataframes in one go, it is many orders of magnitude faster
    # than selecting specific rows.  Merge can then be done in pandas.
    hotspot_chunks = pd.read_sql_table("hotspots",
                                       task_args['legacy_engine'],
                                       chunksize=50000,
                                       columns=['grid_x', 'grid_y', 'year', 'month', 'day',
                                                'hour', 'minute', 'frp', 'swir_16', 'swir_22'])
    hotspot_df = utils.process_chunks(hotspot_chunks)

    sampling_chunks = pd.read_sql_table("sampling",
                                        task_args['legacy_engine'],
                                        chunksize=50000,
                                        columns=['grid_x', 'grid_y', 'year', 'month', 'day',
                                                 'hour', 'minute', 'local_cloudiness'])
    sampling_df = utils.process_chunks(sampling_chunks)

    for row in persistent_df.itertuples():
        hotspot_sub_df = hotspot_df[(hotspot_df.grid_x == row.grid_x) & (hotspot_df.grid_y == row.grid_y)]
        sampling_sub_df = sampling_df[(sampling_df.grid_x == row.grid_x) & (sampling_df.grid_y == row.grid_y)]

        merged_df = data.upsample_dataframes(hotspot_sub_df, sampling_sub_df)

        data.dump_activity_to_lines(merged_df.to_dict(orient='records'))
