from abc import ABC, abstractmethod
import pandas as pd
import numpy as np
from scipy.interpolate import RectBivariateSpline
from scipy.ndimage import convolve, zoom
from datetime import timezone

import service_constants as constants
from resources import slstr_pixel_size


class BaseDetector(ABC):

    def __init__(self,
                 day_night_angle: float = None,
                 swir_thresh: float = None,
                 cloud_window_size: int = None) -> None:

        """
        BaseDetector base class that contains all the shared attributes
        and methods used by the child classes that inherit from it.

        Args:
            day_night_angle: Solar zenith angle that defines the day/night boundary
            swir_thresh: Threshold which hotspots must exceed to be detected
            cloud_window_size: Image window over which local cloud statistics are computed
        """

        self.day_night_angle = day_night_angle
        self.swir_thresh = swir_thresh
        self.cloud_window_size = cloud_window_size
        self.sensor = None

        # setup attributes generated
        self.latitude = None
        self.longitude = None
        self.sza = None
        self.night_mask = None
        self.swir_16 = None
        self.swir_22 = None
        self.cloud_free = None
        self.cloudy = None
        self.local_cloudiness = None
        self.potential_hotspots = None
        self.sensor = None
        self.pixel_size = None
        self.frp = None
        self.hotspots = None
        self.aggregated_hotspots = None
        self.datetime_info = None

    def _make_night_mask(self) -> None:
        """
        Computes the day/night binary mask from
        the solar zenith angle.

        Returns:
            None
        """
        self.night_mask = self.sza >= self.day_night_angle

    def _compute_swir_frp(self) -> None:
        """
        Computes the pixel fire radiative power based on the
        Fisher and Wooster SWIR Radiance Method.
        https://doi.org/10.3390/rs10020305

        Returns:
            None
        """
        self.frp = self.pixel_size * constants.FRP_COEFF_SWIR_16[self.sensor] * self.swir_16 / 1000000  # in MW

    def _compute_local_cloudiness(self) -> None:
        """
        Computes the local mean cloudiness from binary cloud masks.

        Returns:
            None
        """
        k = np.ones([self.cloud_window_size, self.cloud_window_size])
        s = convolve(self.cloudy.astype(int), k, mode='constant', cval=0.0)
        count = convolve(np.ones(self.cloudy.shape), k, mode='constant', cval=0.0)
        self.local_cloudiness = s / count

    def _to_dataframe(self, keys: list, sampling: bool = False, joining_df: pd.DataFrame = None) -> pd.DataFrame:
        """
        A flexible dataframe builder that takes in a set of keys that
        correspond to data contained within the object.  For each item
        of data, the samples associated with hotspot activity are selected
        using the provided mask.  The joining_df keyword allows the dataframe
        to be reduced as is sometimes required in the processing chain.

        Args:
            keys: The variables to be included in the dataframe (columns)
            sampling: Flag to determine if hotspot sampling is being evaluated
            joining_df: Used to reduce the dataframe through an Inner Join

        Returns:
            Dataframe containing the requested data defined by the input
            keys, flag, mask and, if included, the reducing dataframe.

        """
        df = pd.DataFrame()

        # store data associated with product
        for k in keys:
            if k not in self.__dict__:
                raise KeyError(k + ' not found in available attributes')
            if self.__dict__[k] is None:
                continue
            if sampling:
                df[k] = self.__dict__[k].flatten()  # Get everything then reduce in the join
            else:
                df[k] = self.__dict__[k][self.aggregated_hotspots]

        # store additional derived data
        if not sampling:
            lines, samples = np.where(self.aggregated_hotspots)
            df['line'] = lines
            df['sample'] = samples
        else:
            lines, samples = self.swir_16.shape
            samples, lines = np.meshgrid(np.arange(samples), np.arange(lines))
            df['line'] = lines.flatten()
            df['sample'] = samples.flatten()
        df['grid_x'] = self._find_arcmin_gridcell(df['longitude'])
        df['grid_y'] = self._find_arcmin_gridcell(df['latitude'])
        for time_period in self.datetime_info:
            df[time_period] = self.datetime_info[time_period]
        df['sensor'] = self.sensor

        # if running sampling then joining df will be provided
        if joining_df is not None:
            # merge to reduce data to only persistent locations
            df = pd.merge(joining_df[['grid_x', 'grid_y']], df, on=['grid_x', 'grid_y'])

            # group to reduce line,sample data to gridded
            df = df.groupby(['grid_x', 'grid_y'], as_index=False).agg({"local_cloudiness": np.mean,
                                                                       "latitude": np.mean,
                                                                       "longitude": np.mean,
                                                                       "year": "first",
                                                                       "month": "first",
                                                                       "day": "first",
                                                                       "hour": "first",
                                                                       "minute": "first",
                                                                       "day_of_year": "first",
                                                                       "days_since_launch": "first",
                                                                       "sensor": "first"})
        return df

    def _detect_potential_hotspots(self, approach: str = None) -> None:

        if approach is None or approach not in ['swir_16_static',
                                                'swir_16_adaptive',
                                                'swir_16_xu'
                                                ]:
            raise ValueError("Hotspot Detection approach not defined")

        if approach == 'swir_16_static':
            self.potential_hotspots = self.swir_16 > self.swir_thresh
        if approach == 'swir_16_adaptive':
            self.potential_hotspots = self.swir_16 > \
                                      np.mean(self.swir_16[self.swir_16 < constants.MIN_RAD]) + \
                                      4 * np.std(self.swir_16[self.swir_16 < constants.MIN_RAD])
        if approach == "swir_16_xu":
            median = np.median(self.swir_16[self.swir_16 > constants.T_VALUE])
            mean = np.mean(self.swir_16[self.swir_16 > constants.T_VALUE])
            s16 = self.swir_16 > (constants.T_VALUE + mean + 2 * median)

            # median = np.median(self.swir_22[self.swir_22 > constants.T_VALUE])
            # mean = np.mean(self.swir_22[self.swir_22 > constants.T_VALUE])
            # s22 = self.swir_22 > (constants.T_VALUE + mean + 2 * median)
            # self.potential_hotspots = s16 & s22
            self.potential_hotspots = s16

    @staticmethod
    def _find_arcmin_gridcell(coordinates: np.array) -> np.array:
        """
        Ingests cartesian coordinates and rescales them
        to an integer representation of a 1-arminute grid.
        This is an approximate representation and is used
        solely for aggregation purposes (i.e. cannot be
        used for visualisation).

        Args:
            coordinates: A set of cartesian coordinates

        Returns:
            An integer value corresponding to an arcminute gridcell

        """
        neg_values = coordinates < 0

        abs_x = np.abs(coordinates)
        floor_x = np.floor(abs_x)
        decile = abs_x - floor_x
        minute = np.around(decile * 60)  # round to nearest arcmin
        minute_fraction = minute * 0.01  # convert to fractional value (ranges from 0 to 0.6)

        max_minute = minute_fraction > 0.59

        floor_x[neg_values] *= -1
        floor_x[neg_values] -= minute_fraction[neg_values]
        floor_x[~neg_values] += minute_fraction[~neg_values]

        # deal with edge cases - just round them all up
        if np.sum(max_minute) > 0:
            floor_x[max_minute] = np.around(floor_x[max_minute])

        # rescale
        floor_x = (floor_x * 100).astype(int)

        return floor_x

    @abstractmethod
    def _load_arrays(self, subset: dict = None) -> None:
        raise NotImplementedError("Must override _load_arrays")

    @abstractmethod
    def _extract_datetime(self) -> None:
        raise NotImplementedError("Must override _extract_datetime")

    @abstractmethod
    def run_detector(self, subset: dict = None) -> None:
        raise NotImplementedError("Must override run_detector")

    @abstractmethod
    def to_dataframe(self, keys: list, joining_df: pd.DataFrame = None, sampling: bool = False) -> pd.DataFrame:
        raise NotImplementedError("Must override to_dataframe")


class SLSHotspotDetector(BaseDetector):

    def __init__(self,
                 product: dict,
                 sensor: str,
                 day_night_angle: float = constants.DAY_NIGHT_ANGLE,
                 swir_thresh: float = constants.SLS_SWIR_THRESHOLD,
                 cloud_window_size: int = constants.SLS_CLOUD_WINDOW_SIZE) -> None:
        """
        Detector implementation for the Sea and Land Surface Temperature Scanning (SLSTR)
        radiometer instrument series.
        Args:
            product: SLSTR data product
            day_night_angle: Solar zenith angle that defines the day/night boundary
            swir_thresh: Threshold which hotspots must exceed to be detected
            cloud_window_size: Image window over which local cloud statistics are computed
        """
        super().__init__(day_night_angle, swir_thresh, cloud_window_size)
        self.product = product
        self.max_view_angle = constants.SLS_VZA_THRESHOLD  # degrees
        self.sensor = sensor
        self._extract_datetime()

    def _extract_datetime(self) -> None:
        dt_info = pd.Timestamp(self.product['time_an'].start_time)
        dt_launch = pd.Timestamp(year=2016, month=2, day=16, tz=timezone.utc)
        self.datetime_info = {'year': dt_info.year,
                              'month': dt_info.month,
                              'day': dt_info.day,
                              'hour': dt_info.hour,
                              'minute': dt_info.minute,
                              'day_of_year': dt_info.dayofyear,
                              'days_since_launch': int((dt_info - dt_launch).total_seconds() / (60 * 60 * 24))}

    def _load_arrays(self, subset: dict = None) -> None:
        """
        Loads the product data needed for all processing.
        Returns:
            None
        """
        if subset is not None:
            min_y = subset['min_y']
            max_y = subset['max_y']
            min_x = subset['min_x']
            max_x = subset['max_x']
            self.latitude = self.product['geodetic_an']['latitude_an'][min_y:max_y, min_x:max_x]
            self.longitude = self.product['geodetic_an']['longitude_an'][min_y:max_y, min_x:max_x]
            self.swir_16 = self.product['S5_radiance_an']['S5_radiance_an'][min_y:max_y, min_x:max_x].filled(0)
            self.swir_22 = self.product['S6_radiance_an']['S6_radiance_an'][min_y:max_y, min_x:max_x].filled(0)
            self.sza = self._interpolate_array('solar_zenith_tn')[min_y:max_y, min_x:max_x].filled(0)
            self.vza = self._interpolate_array('sat_zenith_tn')[min_y:max_y, min_x:max_x].filled(9999)
            # bayes cloud mask is at 1km resolution, so zoom to 500m
            self.cloud_free_bayes = zoom(self.product['flags_in']['bayes_in'][min_y:max_y, min_x:max_x][:] == 0, 2,
                                         order=0)
            self.cloud_free = self.product['flags_an']['cloud_an'][min_y:max_y, min_x:max_x] == 0
            self.pixel_size = np.tile(np.array(slstr_pixel_size.pixel_size)[min_y:max_y],
                                      (self.vza.shape[0], 1)) * 1000000
        else:
            self.latitude = self.product['geodetic_an']['latitude_an'][:]
            self.longitude = self.product['geodetic_an']['longitude_an'][:]
            self.swir_16 = self.product['S5_radiance_an']['S5_radiance_an'][:].filled(0)
            self.swir_22 = self.product['S6_radiance_an']['S6_radiance_an'][:].filled(0)
            self.sza = self._interpolate_array('solar_zenith_tn').filled(0)
            self.vza = self._interpolate_array('sat_zenith_tn').filled(9999)
            self.cloud_free = self.product['flags_an']['cloud_an'][:] == 0
            # bayes cloud mask is at 1km resolution, so zoom to 500m
            self.cloud_free_bayes = zoom(self.product['flags_in']['bayes_in'][:] == 0, 2, order=0)
            self.pixel_size = np.tile(np.array(slstr_pixel_size.pixel_size), (self.vza.shape[0], 1)) * 1000000
            self._compute_swir_frp()

    def _interpolate_array(self, target: str) -> np.array:
        """
        Interpolates SLSTR data arrays based on cartesian information
        contained within the sensor product using the RectBivariateSpline
        approach.

        Args:
            target: the product to be interpolated

        Returns:
            The interpolated data
        """
        sat_zn = self.product['geometry_tn'][target][:]

        tx_x_var = self.product['cartesian_tx']['x_tx'][0, :]
        tx_y_var = self.product['cartesian_tx']['y_tx'][:, 0]

        an_x_var = self.product['cartesian_an']['x_an'][:]
        an_y_var = self.product['cartesian_an']['y_an'][:]

        spl = RectBivariateSpline(tx_y_var, tx_x_var[::-1], sat_zn[:, ::-1].filled(0))
        interpolated = spl.ev(an_y_var.compressed(),
                              an_x_var.compressed())
        interpolated = np.ma.masked_invalid(interpolated, copy=False)
        sat = np.ma.empty(an_y_var.shape, dtype=sat_zn.dtype)
        sat[np.logical_not(np.ma.getmaskarray(an_y_var))] = interpolated
        sat.mask = an_y_var.mask
        return sat

    def _make_view_angle_mask(self) -> None:
        """
        Screen SLSTR data based on viewing zenith angle so
        that the data is limited to the max view angle (e.g.
        it can be set to 22 degrees to create an ATSR like product).

        Returns:
            None
        """
        self.vza_mask = self.vza <= self.max_view_angle

    def run_detector(self, subset: dict = None) -> None:
        """
        Runs the detector methods on the input data.

        Args:
            subset: specified if processing a subset of the array

        Returns:
            None
        """
        self._load_arrays(subset=subset)
        self._make_night_mask()
        self._make_view_angle_mask()
        self._detect_potential_hotspots(approach='swir_16_xu')
        self.hotspots = self.potential_hotspots & self.night_mask & self.vza_mask
        self.aggregated_hotspots = self.hotspots

    def to_dataframe(self,
                     keys: list,
                     joining_df: pd.DataFrame = None,
                     sampling: bool = False
                     ) -> pd.DataFrame:
        """
        Used to return a dataframe containing all needed information
        on the hotspots detected during the run_detector call.  The information
        returned is dependent on the keys provided, and if needed can be screened
        using the joining_df keyword arg.
        Args:
            keys: The data required in the dataframe
            joining_df: An joining dataframe that can is used to reduce the hotspots.
            sampling: Flag to determine if hotspot sampling is being evaluated

        Returns:
            A dataframe containing the requested data.

        """
        if not ('latitude' in keys and 'longitude' in keys):
            raise KeyError('At a minimum, latitude and longitude keys are required')
        return self._to_dataframe(keys)


class SLSSamplingDetector(SLSHotspotDetector):

    def __init__(self,
                 product: dict,
                 sensor: str,
                 day_night_angle: float = constants.DAY_NIGHT_ANGLE,
                 swir_thresh: float = constants.SLS_SWIR_THRESHOLD,
                 cloud_window_size: int = constants.SLS_CLOUD_WINDOW_SIZE) -> None:
        """
        Detector implementation for the Sea and Land Surface Temperature Scanning (SLSTR)
        radiometer instrument series.  Used to extract persistent hotspots and hotspot
        location sampling.
        Args:
            product: SLSTR data product
            day_night_angle: Solar zenith angle that defines the day/night boundary
            swir_thresh: Threshold which hotspots must exceed to be detected
            cloud_window_size: Image window over which local cloud statistics are computed
        """
        super().__init__(product, sensor, day_night_angle, swir_thresh, cloud_window_size)

    def _load_latlon(self) -> None:
        """
        Loads latitude and longitude arrays for use in precheck

        :return: None
        """
        self.latitude = self.product['geodetic_an']['latitude_an'][:]
        self.longitude = self.product['geodetic_an']['longitude_an'][:]

    def process_pre_check(self, joining_df: pd.DataFrame = None) -> bool:
        """
        Checks the input data to see whether it is collocated with persistent
        thermal anomalies that are contained in the joining dataframe.

        :param joining_df: dataframe of one arcminute lat and lon grid cell
        :return: boolean defining if fill should be processed or not
        """

        if joining_df is None:
            raise AttributeError('A joining_df must be provided for prechecking')

        self._load_latlon()

        df = pd.DataFrame()
        for k in ['latitude', 'longitude']:
            if k not in self.__dict__:
                raise KeyError(k + ' not found in available attributes')
            if self.__dict__[k] is None:
                continue
            df[k] = self.__dict__[k].flatten()
        df['grid_x'] = self._find_arcmin_gridcell(df['longitude'])
        df['grid_y'] = self._find_arcmin_gridcell(df['latitude'])

        df = pd.merge(joining_df[['grid_x', 'grid_y']], df, on=['grid_x', 'grid_y'])
        if df.empty:
            return False
        else:
            return True

    def run_detector(self, subset: dict = None) -> None:
        """
        Runs the detector methods on the input data. Runs additional
        pixel characterisation over the parent class.

        Args:
            subset: specified if processing a subset of the array

        Returns:
            None
        """
        super().run_detector(subset=subset)
        self.cloudy = ~self.potential_hotspots & ~self.cloud_free_bayes & self.night_mask & self.vza_mask
        self._compute_local_cloudiness()

    def to_dataframe(self,
                     keys: list,
                     joining_df: pd.DataFrame = None,
                     sampling: bool = False) -> pd.DataFrame:
        """
        Used to return a dataframe containing all needed information
        on presistent hotspots detected during the run_detector call.
        The information returned is dependent on the keys provided,
        and is screened using the joining_df keyword arg.
        Args:
            keys: The data required in the dataframe
            joining_df: An joining dataframe that can is used to reduce the hotspots.
            sampling: Flag to determine if hotspot sampling is being evaluated

        Returns:
            A dataframe containing the requested and reduced data.

        """
        if not ('latitude' in keys and 'longitude' in keys):
            raise KeyError('At a minimum, latitude and longitude keys are required')
        if joining_df is None:
            raise AttributeError('A joining_df must be provided for data reduction')
        return self._to_dataframe(keys, sampling=sampling, joining_df=joining_df)
