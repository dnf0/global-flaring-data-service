import os
import time
import json

import pandas as pd
import numpy as np
import logging
from dataclasses import dataclass, field
from typing import Any
import datetime

import service_constants


@dataclass(order=True)
class PrioritizedTask:
    priority: int
    f: Any = field(compare=False)
    data: Any = field(compare=False, default=None)


@dataclass()
class BatchItem:
    id: int
    path: str
    file: str
    populated: int
    attempts: int
    last_sampled_unix_time: int


def wrangle_for_unseen(unseen: list) -> list:
    rows = []
    for file_path in unseen:
        rows.append({'id': None,
                     'file': file_path.file,
                     'path': file_path.path,
                     'time_added': datetime.datetime.now(),
                     'populated': 0,
                     'attempts': 0,
                     'last_process_attempt_unix_time': 0,
                     'last_sampled_unix_time': 0}
                    )
    return rows


def wrangle_for_batch(rows: list) -> list:
    output = []
    for row in rows:
        output.append(BatchItem(id=row['id'],
                                path=row['path'],
                                file=row['file'],
                                populated=int(row['populated']),
                                attempts=row['attempts'],
                                last_sampled_unix_time=row['last_sampled_unix_time']
                                )
                      )
    return output


def wrangle_for_sampling(rows: list) -> list:
    output = []
    for row in rows:
        output.append({'id': row.id, "last_sampled_unix_time": time.time()})
    return output


def wrangle_for_hotspot_attempt(rows: list) -> list:
    output = []
    for row in rows:
        output.append({'id': row.id, "attempts": row.attempts + 1, "last_process_attempt_unix_time": time.time()})
    return output


def wrangle_for_hotspot_population(rows: list) -> list:
    output = []
    for row in rows:
        output.append({'id': row.id, "populated": 1})
    return output


def orbit_detection_to_month_detection(df: pd.DataFrame, subset_cols: list = None) -> pd.DataFrame:
    """
    Reduce orbital level data to a monthly product that
    flags arcminute resolution grid cells with at least
    one detection in any given month.
    Args:
        df: dataframe with all orbit information
        subset_cols: columns to use in evaluation

    Returns:
        Reduced dataframe indiciating grid cells with at least
        one hotspot detection in any given month.

    """
    if subset_cols is None:
        subset_cols = ['grid_x', 'grid_y', 'year', 'month']
    return df.drop_duplicates(subset=subset_cols)


def month_detection_to_annual_count(df: pd.DataFrame) -> pd.DataFrame:
    """
    Given a dataframe containing monthly detections returns
    an aggregated dataframe providing the total number of hotspot
    detections counted in a given gridcell over an approximate
    twelve month period.

    Args:
        df: Monthly detection dataframe

    Returns:
        Aggregated dataframe of annum hotspot counts

    """
    df['day'] = 1  # arbitrary day column to allow use of pd.to_datetime
    df['dt'] = pd.to_datetime(df[['year', 'month', 'day']])
    df['counter'] = 1

    count_df_list = []

    # iterate over unique datetimes
    for start_dt in sorted(df['dt'].unique()):
        # create approximate 12 month period (~365 days)
        stop_dt = start_dt + pd.to_timedelta(365, unit='days')

        # subset and aggregate
        sub_df = df[(start_dt <= df.dt) & (df.dt < stop_dt)]
        sub_df = sub_df.groupby(['grid_x', 'grid_y'], as_index=False).agg({'counter': np.sum})

        count_df_list.append(sub_df)

    count_df = pd.concat(count_df_list)
    count_df = count_df[count_df.counter >= service_constants.COUNT_FOR_PERSISTENCE]
    count_df.drop_duplicates(inplace=True, subset=['grid_x', 'grid_y'])

    return count_df


def dump_persistent(df: pd.DataFrame) -> None:
    """
    Attempts to populate database with persistent data

    :return: None
    """
    try:

        logging.debug(f"Dataframe contains {df.shape[0]} hotspot locations")
        df = orbit_detection_to_month_detection(df)
        logging.debug(f"Dataframe contains {df.shape[0]} monthly locations")
        df = month_detection_to_annual_count(df)
        logging.debug(f"Dataframe contains {df.shape[0]} persistent locations")

        df.to_csv(os.path.join(os.environ.get("OUTPUT_ROOT"), "persistent.csv"))
    except ValueError:
        # If the database is empty the persistent file generation will fail.  Temporarily
        # dump an empty db to avoid errors.  It will be updated as data enters the db.
        logging.warning("Persistent Dataframe generation failed due to ValueError, dumping empty df")
        df = pd.DataFrame()
        df.to_csv(os.path.join(os.environ.get("OUTPUT_ROOT"), "persistent.csv"))


def upsample_dataframes(hotspot: pd.DataFrame, sampling: pd.DataFrame) -> pd.DataFrame:
    """
    Takes in hotspot and sampling dataframes and upsamples them to daily observations.  Only
    days with observation contain values, days with no values are set to NaN.  The nansum method
    is then used to aggregate the daily data to 365 day totals.

    365d totals of frp and observations are produced from the hotspot and sampling dataframes
    respectively. Daily data is then merged and used to produce normalised daily frp values
    based on the previous 365 days of activity and expected cloud free overpasses.

    The expected cloud free overpass data is retained as can be used to provide confidence
    on the hotspots as a statistical metric (e.g. x observations required to give x% confidence).

    :param hotspot:  dataframe containing hotspot data
    :param sampling:  dataframe containing sampling data
    :return:
    """

    # timestamp represents an atomic satellite overpass
    hotspot['timestamp'] = pd.to_datetime(hotspot[['year', 'month', 'day', 'hour', 'minute']])
    sampling['timestamp'] = pd.to_datetime(sampling[['year', 'month', 'day', 'hour', 'minute']])

    # for each timestamp there can be multiple associated hotspot pixels as multiple pixels
    # might compose a single grid cell. Hence the need for grouby and totalling of FRP
    grouped_hotspot = hotspot.groupby('timestamp', as_index=False)['frp'].agg(np.sum)

    # calculate average cloudiness for each the persistent grid cell at each timestamp.
    # sampling retains sub-gridcell cloud information, hence groupby over grid cell.
    grouped_sampling = sampling.groupby('timestamp', as_index=False)['local_cloudiness'].agg(np.mean)
    assert sampling.shape[0] == grouped_sampling.shape[0]
    grouped_sampling['overpasses'] = 1

    upsampled_hotspot = grouped_hotspot.set_index('timestamp').resample('D').aggregate(np.mean)
    rolling365_daily_hotspot = upsampled_hotspot.rolling('365D').sum()

    upsampled_sampling = grouped_sampling.set_index('timestamp').resample('D').aggregate(
        {'overpasses': np.sum, 'local_cloudiness': np.mean})
    rolling_overpasses = upsampled_sampling['overpasses'].rolling('365D').sum()
    rolling_local_cloudiness = upsampled_sampling['local_cloudiness'].rolling('365D').mean()
    rolling365_daily_sampling = pd.concat([rolling_overpasses, rolling_local_cloudiness], axis=1)

    # merge upsampled aggregated data on timestamps
    rolling365_daily_sampling.reset_index(inplace=True)
    rolling365_daily_hotspot.reset_index(inplace=True)
    merged = pd.merge(rolling365_daily_sampling, rolling365_daily_hotspot, on='timestamp')

    merged['expected_cloud_free_overpasses'] = (merged.overpasses * (1 - merged.local_cloudiness))
    # TODO div zero errors?  Will there be any, as to have FRP there must be at least 1 overpass...
    merged['normalised_frp'] = merged.frp / merged.expected_cloud_free_overpasses

    # add in median hotspot ratio and grid information
    merged['median_swir_ratio'] = np.median(hotspot.swir_16 / hotspot.swir_22)
    merged['grid_x'] = hotspot.grid_x.iloc[0]
    merged['grid_y'] = hotspot.grid_y.iloc[0]

    # convert timestamp to string for future json dump
    merged['timestamp'] = merged['timestamp'].dt.strftime("%Y%m%d")

    to_return = ['timestamp',
                 'grid_x',
                 'grid_y',
                 'normalised_frp',
                 'expected_cloud_free_overpasses',
                 'median_swir_ratio']
    return merged[to_return]


def dump_activity_to_lines(items: list) -> None:
    with open(os.path.join(os.environ.get("XFC_ROOT"), service_constants.JSONL_FNAME), "a+") as f:
        for item in items:
            json.dump(item, f)
            f.write(os.linesep)
