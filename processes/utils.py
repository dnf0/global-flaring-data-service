import logging
import os
import pickle
import shutil
import subprocess
import tempfile
import time
import zipfile
import re
from collections import namedtuple

import pandas as pd
from netCDF4 import Dataset

import service_constants
from scripts.run_batch import run


def unzipper(input_zip: str, path_to_temp: str) -> dict:
    """
    Unzips inputs SLSTR data and extracts required spectral and
    meta data for further processing.

    :param input_zip: path to slstr zip file
    :param path_to_temp: path to temporary extraction folder
    :return: dictionary containing unzipped data
    """
    data_dict = {}
    to_extract = ["S5_radiance_an.nc", "S6_radiance_an.nc",
                  "geodetic_an.nc", "geometry_tn.nc",
                  "cartesian_an.nc", "cartesian_tx.nc",
                  "indices_an.nc", "flags_an.nc", "flags_in.nc",
                  "time_an.nc"]
    logging.debug(f"Unzipping input file {input_zip}")
    with zipfile.ZipFile(input_zip) as nc_file:
        for name in nc_file.namelist():
            split_name = name.split('/')[-1]
            if split_name in to_extract:
                var_name = split_name.split('.')[0]
                source = Dataset(nc_file.extract(name, path_to_temp))
                data_dict[var_name] = source

    # remove the unzip files
    logging.debug("Removing unzipped file")
    dir_to_remove = os.path.join(path_to_temp, input_zip.split('/')[-1].replace('zip', 'SEN3'))
    if os.path.isdir(dir_to_remove):  # test if the path points to a directory
        shutil.rmtree(dir_to_remove, ignore_errors=True)
    else:  # normal file
        os.remove(dir_to_remove)
    return data_dict


def submit_batch(batch_list_file_path: str, batch_size: int) -> None:
    script_dir = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        "..",
        'scripts/'
    )

    (gd, temp) = tempfile.mkstemp(".sbatch", "ggf_", os.environ.get("BATCH_ROOT"), True)
    g = os.fdopen(gd, "w")
    g.write('#!/bin/bash\n')
    g.write(f'export PYTHONPATH=$PYTHONPATH:{os.environ.get("PYTHON_PROJECT_DIR")}\n')
    g.write(f"SAMPLE_LIST=($(<{batch_list_file_path}))\n")
    g.write("SAMPLE=${SAMPLE_LIST[${SLURM_ARRAY_TASK_ID}]}\n")
    g.write(f"{''.join([script_dir, 'run_batch.py'])} $SAMPLE\n")
    g.write(" ".join(["rm -f ", temp + "\n"]))
    g.close()
    os.chmod(temp, 0o755)

    # make dir for logging outputs
    batch_filename = batch_list_file_path.split('/')[-1].split('.')[0]
    slurm_log_dir = os.path.join(os.environ.get("LOGGING_ROOT"), batch_filename)
    if not os.path.exists(slurm_log_dir):
        os.mkdir(slurm_log_dir)

    cmd = ['sbatch', '-p', 'short-serial-4hr', '--account=short4hr',
           '-o', os.path.join(slurm_log_dir, '%A_%a.out'),
           '-e', os.path.join(slurm_log_dir, '%A_%a.err'),
           '--job-name', 'ggf_service',
           '--array', f'0-{batch_size-1}',
           temp]

    try:
        subprocess.run(cmd)
    except Exception as e:
        logging.exception('Subprocess batch call failed with error:', e)


def submit_sequential(batch_list_file_path: str):
    with open(batch_list_file_path, "r+") as f:
        items_to_process = f.readlines()
        logging.debug(f"Extracted data {items_to_process}")

    for item in items_to_process:
        path, file, hotspot_populated = item.rstrip().split(service_constants.BATCH_SEPERATOR)
        if "sentinel3a" in path:
            sensor = "S3A"
        elif "sentinel3b" in path:
            sensor = "S3B"
        logging.debug(f"Running script with {path, file, sensor, hotspot_populated}")
        run(path, file, sensor, int(hotspot_populated))


def retry_dataframe_loading(csv_file_path):
    """
    Retries loading csv file to dataframe.  Retries
    are used as sometimes the file may have been
    created on file system, but not immediately populated, so
    will fail.  This deals with that issue.

    :param csv_file_path: path to csv file
    :return:
    """
    for attempt in range(5):
        try:
            return pd.read_csv(csv_file_path, index_col=[0])
        except pd.errors.EmptyDataError:
            logging.warning(f"Could not load dataframe {csv_file_path}... retrying attempt {attempt}")
            time.sleep(1)
    return


def define_batch_process_output_path(input_path: str, output_root: str) -> str:
    """
    For a given NEODC input data path extrac the required
    temporal information and use to generate the output path
    when batch processed outputs will be stored.

    :param input_path: NEODC SLSTR product path
    :param output_root: Batch processing output root
    :return: Batch processing output filepath
    """
    # separate file from path
    y, m, d = input_path.split('/')[-3:]
    output_path = os.path.join(output_root, y, m, d)
    logging.debug(f"Defining output path for {input_path} as {output_path}")
    if not os.path.exists(output_path):
        # catch race conditions
        try:
            logging.debug(f"Creating output path {output_path}")
            os.makedirs(output_path)
        except FileExistsError:
            logging.debug(f"Output path {output_path} already exists")
            pass
    return output_path


def generate_batch_list_file(batch_files: list) -> str:
    """
    Writes list of batch filenames to file for processing

    :param batch_files: list of batch filenames
    :return: the temp file path containing the files to be processed
    """
    (gd, temp) = tempfile.mkstemp(".list", "ggf_", os.environ.get("BATCH_ROOT"), True)
    with open(temp, "w") as f:
        for file_details in batch_files:
            args = service_constants.BATCH_SEPERATOR.join([file_details.path,
                                                           file_details.file,
                                                           str(file_details.populated) + '\n'])
            f.write(args)
            logging.debug(f"""Created temporary batch file {temp} with
                            data: args""")
    return temp


def batch_process_count() -> int:
    if not shutil.which("sbatch"):
        return 0
    else:
        squeue = subprocess.Popen(("squeue",
                                   "-u",
                                   os.environ.get("SLURM_USERNAME"),
                                   "-h",
                                   "-t",
                                   "pending,running",
                                   "-r"), stdout=subprocess.PIPE)
        return int(subprocess.check_output(('wc', '-l'), stdin=squeue.stdout))


def batch_split(to_split: list, target_size: int = 1000) -> (list, list):
    """
    Given a input list split into lists of the target size.

    :param to_split: files to be splits into lists
    :param target_size: target number of files per list
    :return: batches - a list of batch file lists; batch_size - a list of file counts in each batch
    """
    batches, batch_sizes = [], []

    batch_count, final_batch_size = divmod(len(to_split), target_size)
    start = 0
    for b in range(1, batch_count+1):
        batches.append(to_split[start:b * target_size])
        batch_sizes.append(b * target_size - start)
        start = b * target_size
    if final_batch_size:
        batches.append(to_split[start:])
        batch_sizes.append(final_batch_size)
    logging.debug(f"Generated file split with sizes {batch_sizes}")
    return batches, batch_sizes


def dump_seen(session, stmt) -> None:
    try:
        with open(os.path.join(os.environ.get('OUTPUT_ROOT'), "seen.pickle"), 'rb') as f:
            seen = pickle.load(f)
    except FileNotFoundError:
        seen = set()

    for row in session.execute(stmt):
        # form row into ymd format
        seen.add(re.search("[0-9]{4}/[0-9]{2}/[0-9]{2}", row[0]).group(0))

    # pickle fileset
    with open(os.path.join(os.environ.get('OUTPUT_ROOT'), "seen.pickle"), 'wb') as f:
        pickle.dump(seen, f)


def find_unseen_files() -> list:

    file_path = namedtuple("file_path", "path file")

    try:
        with open(os.path.join(os.environ.get('OUTPUT_ROOT'), "seen.pickle"), 'rb') as f:
            seen = pickle.load(f)
    except FileNotFoundError:
        raise

    unseen = []
    for sensor_dir in ["sentinel3a", "sentinel3b"]:
        to_walk = os.path.join(os.environ.get('SAT_DATA_ROOT'), sensor_dir, 'data', 'SLSTR')
        for root, dirs, files in os.walk(to_walk,
                                         topdown=True,
                                         followlinks=True):
            if "SLSTR" not in root:
                continue
            dirs.sort(reverse=True)

            # check if root contain a ymd stamp
            ymd_match = re.search("[0-9]{4}/[0-9]{2}/[0-9]{2}", root)
            if not ymd_match:
                continue

            # check if ymd not in seen
            ymd = ymd_match.group(0)
            if ymd not in seen:
                logging.info(f"{ymd} not in seen: adding all contained files to unseen")
                for f in files:
                    # process only NT (non-time critical) files
                    if f.endswith('.zip') and ("NT" in f):
                        unseen.append(file_path(root, f))
            else:
                logging.info("Found no new files on the system")
                return unseen  # if in seen then assume processed all earlier files
    logging.info(f"Found {len(unseen)} new files on the system")
    return unseen


def process_chunks(chunks) -> pd.DataFrame:
    """
    Helper function to convert chunked dataframes
    into a single dataframe
    :param chunks:
    :return:
    """
    chunk_list = list(chunks)
    if len(chunk_list) > 1:
        return pd.concat(chunk_list)
    else:
        return chunk_list[0]
