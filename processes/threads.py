import os
import sys
import time
import shutil
import logging
import threading
from collections import deque

import processes.data as data_processes
import processes.utils as utils
import processes.tasks as tasks
import service_constants
from service_constants import TASK_QUEUES


# TODO test
def db_queue_manager(engine, legacy_engine) -> None:
    """
    Manages all database interactions.

    As SQLite is being used as db only single write processes can
    occur.  This queue manager ensures that only a single process
    (read or write) is occuring on the database at a given moment.

    It listens to the db queue for tasks.  When a task arrives it
    uses the associated process flag to determine how to deal with
    it.  Each task in the db queue is a PrioritsedItem dataclass
    (see data.py).

    :param engine: sqlalchemy2 engine
    :param legacy_engine: sqlaclchemy1.4 engine (used for pandas interactions)
    :return: None
    """

    args = {
        'engine': engine,
        'legacy_engine': legacy_engine,
        'data': None
    }

    while True:

        # logging information
        if TASK_QUEUES['db'].empty():
            continue

        if not TASK_QUEUES['db'].qsize() % 1:  # redundant now, but change mod later
            logging.info(f"{TASK_QUEUES['db'].qsize()} Task(s) remain(s) to be processed in TASK_QUEUES['db']...")

        logging.debug(f"Current number of active threads: {threading.active_count()}")
        current_task = TASK_QUEUES['db'].get_nowait()
        logging.info(f"Working on task: {current_task.f}")

        # each task is just a function to run
        args['data'] = current_task.data
        current_task.f(args)


def add_select_seen_task_to_db_queue() -> None:
    """
    Adds a dump_seen task to the db queue.

    This results in the queue manager dumping all
    filenames in the db to a pickle file.
    Which is used to determine which files have
    already been ingested into the db.

    Runs once per day.  Completion instigates
    insert unseen and dump activity tasks to
    be added to db queue.
    :return:
    """
    while True:
        logging.debug("Working on adding dump_seen task to db queue")
        task = data_processes.PrioritizedTask(priority=service_constants.DUMP_SEEN_PRIORITY,
                                              f=tasks.select_seen)
        TASK_QUEUES['db'].put(task)

        duration = 3600 * 24
        logging.debug(f"Added task to dump seen files from DB...  Sleeping for {duration} seconds")
        time.sleep(duration)


def add_insert_unseen_files_task_to_db_queue() -> None:
    """
    Adds an insert_unseen task to the dq queue.

    The insert adds all files new to the system to the db.
    It does so in batches to avoid excessively large writes.

    Only runs after update_unseen queue has been populated,
    which is dependent on completion of a dump_seen task.

    Completion instigates select_files task to be added to
    db queue

    :return: None
    """
    while True:

        # wait for add_update_seen_stmt_to_db_queue() to run before processing
        while TASK_QUEUES['update_unseen'].empty():
            duration = 10
            logging.debug(f"No items in TASK_QUEUES['update_unseen']...  Sleeping for {duration} seconds")
            time.sleep(duration)
            continue
        logging.debug("Working on adding insert_unseen task to db queue")
        TASK_QUEUES['update_unseen'].get()

        # split unseen file paths into chunks and generate bulk insert statements
        task = data_processes.PrioritizedTask(priority=service_constants.INSERT_UNSEEN_PRIORITY,
                                              f=tasks.insert_unseen)
        TASK_QUEUES['db'].put(task)


def add_select_files_for_batch_proc_task_to_db_queue() -> None:
    """
    Adds a select_files task to the db queue.

    Selects all files that have not been processed
    for hotspots or need processing for sampling.

    Only runs after select_batch queue has been populated
    with a flag which is dependent on completion of an
    insert_unseen task.

    :return: None
    """
    while True:
        while TASK_QUEUES['select_batch'].empty():
            duration = 10
            logging.debug(f"No items in TASK_QUEUES['select_batch']...  Sleeping for {duration} seconds")
            time.sleep(duration)
            continue
        logging.debug("Working on adding select_files task to db queue")
        TASK_QUEUES['select_batch'].get()

        task = data_processes.PrioritizedTask(priority=service_constants.SELECT_FOR_BATCH_PRIORITY,
                                              f=tasks.select_batch)
        TASK_QUEUES['db'].put(task)


def submit_batch_process() -> None:
    """
    Watches batch queue for new batch tasks.

    Each batch task queue item consists of list of BatchItem, with
    a batch item representing a single file to be processed.

    The main responsibility of the function is to send groups of
    BatchItems for bulk processing (if slurm available).

    Each BatchItem is also sent to the update_sample and (conditionally)
    the update_hotspot queues, which check the file system for processing
    completion.  For all BatchItems sent to the update_hotspot queue an
     update_hotspot_attempt_unix_time task is sent to the db queue to update
    the db.

    :return: None
    """
    while True:

        if TASK_QUEUES['batch'].empty():
            duration = 10
            logging.debug(f"No items in TASK_QUEUES['batch']...  Sleeping for {duration} seconds")
            time.sleep(duration)
            continue

        # split jobs into sampling only and sampling and hotspot
        jobs = TASK_QUEUES['batch'].get()

        # refine jobs
        epoch_time = int(time.time())
        sampling_jobs = [j for j in jobs if
                         epoch_time - j.last_sampled_unix_time >= service_constants.PERSISTENT_PERIOD]
        hotspot_jobs = [j for j in jobs if not j.populated]

        logging.info(f"{len(sampling_jobs)} sampling jobs to run")
        logging.info(f"{len(hotspot_jobs)} hotspot jobs to run")
        logging.info(f"{len(jobs)} all jobs to run")

        # update sampling and (conditionally) hotspot queues
        TASK_QUEUES['update_sample'].put(sampling_jobs)
        if hotspot_jobs:
            TASK_QUEUES['update_hotspot'].put(hotspot_jobs)
            # update approximate processing time and attempt
            logging.debug("Working on adding update_hotspot_attempt_unix_time task to db queue")
            task = data_processes.PrioritizedTask(priority=service_constants.UPDATE_ATTEMPT_PRIORITY,
                                                  f=tasks.update_attempt,
                                                  data=hotspot_jobs)
            TASK_QUEUES['db'].put(task)

        # split jobs and send for processing
        split_jobs, job_sizes = utils.batch_split(jobs)
        logging.info(f"Generating {len(job_sizes)} batch jobs with max size: {max(job_sizes)}")
        batch_file_paths = [utils.generate_batch_list_file(j) for j in split_jobs]

        for batch_file_path, job_size in zip(batch_file_paths, job_sizes):
            if not shutil.which("sbatch") or "pytest" in sys.modules:
                utils.submit_sequential(batch_file_path)
            else:
                # TODO add testing for batch submission
                utils.submit_batch(batch_file_path, job_size)


def add_insert_hotspot_data_task_to_db_queue() -> None:
    """
    Watches update_hotspot queue for new tasks that have been sent for batch processing.

    Each task is a list of BatchItems that contain information about a file
    submitted for batch processing.  The list is converted to a jobs queue
    to facilitate processing.

    If an output file exists for a given BatchItem then two db tasks are produced,
    one to update the data in the db and the other to update the population
    status for the file.

    If the output file does not exist then the BatchItem is added back to the jobs queue
    to wait for the associated file to finish processing.

    :return: None
    """
    while True:
        if TASK_QUEUES['update_hotspot'].empty():
            time.sleep(60)  # Sleep regularly while wiating for jobs to come in
            continue

        jobs = deque(TASK_QUEUES['update_hotspot'].get_nowait())
        n_jobs = len(jobs)
        n_jobs_comp = 0
        logging.debug(f"Working on adding update_hotspot tasks to db queue for {len(jobs)} jobs")
        complete_jobs = []
        while jobs:

            job = jobs.pop()

            path = utils.define_batch_process_output_path(job.path, os.environ.get('OUTPUT_ROOT'))
            file = job.file.replace('.zip', service_constants.HOTSPOT_SUFFIX)
            csv_file_path = os.path.join(path, file)
            if not os.path.isfile(csv_file_path):
                jobs.appendleft(job)
                continue

            complete_jobs.append(job)
            n_jobs_comp += 1

            task_df = data_processes.PrioritizedTask(priority=service_constants.INSERT_DATA_PRIORITY,
                                                     f=tasks.insert_hotspot_data,
                                                     data=job)
            TASK_QUEUES['db'].put(task_df)

            # Bulk update population status
            n_complete = len(complete_jobs)
            if n_complete % 250 == 0 or not len(jobs):
                logging.info(f"Added {n_complete} insert_hotspot_data jobs to db queue")
                logging.info(f"{n_jobs_comp} out of {n_jobs} batch procced ({round(n_jobs_comp / n_jobs * 100, 2)}%)")
                # update population completion
                task = data_processes.PrioritizedTask(priority=service_constants.UPDATE_POPULATION_PRIORITY,
                                                      f=tasks.update_population,
                                                      data=complete_jobs)
                TASK_QUEUES['db'].put(task)
                complete_jobs = []


def add_insert_sampling_data_task_to_db_queue() -> None:
    """
    Watches update_sample queue for new tasks that have been sent for batch processing.

    Each task is a list of BatchItems that contain information about a file
    submitted for batch processing.  The list is converted to a jobs queue
    to facilitate processing.

    If an output file exists for a given BatchItem then one db tasks is
    produced to update the sampling data in the db.

    If the output file does not exist then the task is added back to the jobs queue
    to wait for the associated file to finish processing.

    :return: None
    """
    while True:
        if TASK_QUEUES['update_sample'].empty():
            time.sleep(60)  # sleep while waiting for job to come in
            continue

        jobs = deque(TASK_QUEUES['update_sample'].get_nowait())
        n_jobs = len(jobs)
        n_jobs_comp = 0
        logging.debug(f"Working on adding update_sample tasks to db queue for {len(jobs)} jobs")
        complete_jobs = []
        while jobs:

            job = jobs.pop()

            path = utils.define_batch_process_output_path(job.path, os.environ.get('OUTPUT_ROOT'))
            file = job.file.replace('.zip', service_constants.SAMPLING_SUFFIX)
            csv_file_path = os.path.join(path, file)

            if not os.path.isfile(csv_file_path):
                jobs.appendleft(job)
                continue

            # check if file has not yet been updated
            file_proc_time = int(os.path.getmtime(csv_file_path))
            if file_proc_time == job.last_sampled_unix_time:
                jobs.appendleft(job)
                continue

            job.last_sampled_unix_time = file_proc_time
            complete_jobs.append(job)
            n_jobs_comp += 1

            task_df = data_processes.PrioritizedTask(priority=service_constants.INSERT_DATA_PRIORITY,
                                                     f=tasks.insert_sample_data,
                                                     data=job)
            TASK_QUEUES['db'].put(task_df)

            # Bulk update population status.
            n_complete = len(complete_jobs)
            if n_complete % 250 == 0 or not len(jobs):
                logging.info(f"Added {n_complete} insert_sample_data jobs to db queue")
                logging.info(f"{n_jobs_comp} out of {n_jobs} batch procced ({round(n_jobs_comp / n_jobs * 100, 2)}%)")
                # update population completion
                task = data_processes.PrioritizedTask(priority=service_constants.UPDATE_SAMPLING_UNIX_TIME_PRIORITY,
                                                      f=tasks.update_sampling,
                                                      data=complete_jobs)

                TASK_QUEUES['db'].put(task)
                complete_jobs = []


def add_persistent_extraction_to_db_queue() -> None:
    """
    Once per persistent period submits a prioritised task to the db queue
    to identify persistent locations.
    :return:
    """
    while True:
        # check if persistent does not exist or last processing time was less than persistent period ago
        if os.path.isfile(os.path.join(os.environ.get("OUTPUT_ROOT"), "persistent.csv")):
            last_process_time = int(os.path.getmtime(os.path.join(os.environ.get("OUTPUT_ROOT"), "persistent.csv")))
            if time.time() - last_process_time < service_constants.PERSISTENT_PERIOD:
                duration = 24 * 3600
                logging.debug(f"Persistent detection last run within sleep period...  Sleeping for {duration} seconds")
                time.sleep(duration)
                continue
        logging.debug("Working on adding dump_persistent task to db queue")

        task = data_processes.PrioritizedTask(priority=service_constants.DUMP_PERSISTENT_PRIORITY,
                                              f=tasks.dump_persistent)
        TASK_QUEUES['db'].put(task)
        time.sleep(600)  # give task time to complete processing so hopefully only single task sent.  TODO better way?


def add_activity_extraction_to_db_queue() -> None:
    """
    Whenever new files are searched for add a new normalised
    frp extraction process to the db queue to search for new
    hotspots and observations.
    :return:  None
    """
    while True:

        # wait for add_update_seen_stmt_to_db_queue() to run before processing
        while TASK_QUEUES['dump_activity'].empty():
            duration = 10
            logging.debug(f"No items in TASK_QUEUES['dump_activity']...  Sleeping for {duration} seconds")
            time.sleep(duration)
            continue
        TASK_QUEUES['dump_activity'].get()

        logging.info("Working on adding dump_activity task to db queue")
        task = data_processes.PrioritizedTask(priority=service_constants.DUMP_ACTIVITY_PRIORITY,
                                              f=tasks.dump_activity)
        TASK_QUEUES['db'].put(task)
        time.sleep(600)  # give task time to complete processing so hopefully only single task sent.  TODO better way?


# TODO deal with files in queues (update_hotspot, update_sample) that never get processed.
#  As they are reattched to the queue if file is not found to exist
#  If no batch jobs?
#  Or if seen multiple times when no batch jobs?
#  Line 232(271) deals with this for hotspots(sampling)?  Specifically retry_dataframe_loading
def flush_queues():
    # If no batch jobs, Then can we flush?  Need to lock all other threads so we don't break anything
    # What about sequential running?
    # TASK_QUEUES['update_hotspot']
    # TASK_QUEUES['update_sample']
    pass
