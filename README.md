**Global Flaring Data Service**

This software provides the service component of the global flaring app.  It carries out the detection of thermal anomalies from the Sea and Land Surface Temperature Radiometers (SLSTR) carried on the Sentinel-3 satellites, their partition into persistent and non-persistent groups, and finally assigns all persistent anomalies to classes associated with gas flaring or other non-flaring activity.  The foundation of the approach is defined in [Fisher and Wooster (2018)](https://www.mdpi.com/2072-4292/10/2/305) and it builds on previous work carried out for the Along Track Scanning Radiometer Sensors [Fisher and Wooster (2019)](https://www.sciencedirect.com/science/article/pii/S0034425719303177) and in this [repository](https://gitlab.com/dnf0/kcl-globalgasflaring).

The three core components are:
1. The identification of thermal anomalies uses a straightforward variable threshold based approach describe in [Xu et al.](https://www.sciencedirect.com/science/article/pii/S0034425720303175), where contextual definition of the treshold based on basic image statistics is used.   
2. Detection of unique the thermal anomalies over a given time period (~1 month) in a given cell, and then assesseing total counts over a fixed interval (~12 months) for the window.  If the detection number exceeds a given threshold (typical 4 detections for a 12 month window) then the hotspot is assumed to be persistent.
3. Classification of persistent anomalies into flaring and non-flaring classes.  This is achieved with a spectral ratio method, where the planck curves at two separated wavelengths (here 1.6 µm and 2.2 µm) are used to compute a ratio.  This ratio is monotonically increasing with emitter temperature so can be used to separate hotspots by temperature.  Given that gas flares combust at higher temperatures than nearly all other thermal anomalies observed, this allows thier separation.

The above three processes result in a collection of persistent flaring and non-flaring thermal anomlies.  In this raw form the data can provide some insights into global flaring activity, but the typical metric to evaluate acticity is billions of cubic metres (BCM) of gas flared.  To estimate BCM it is required to know the total amount of energy (J) that a gas flare has emitted over a certain period of time.  Satellite only observe flares on an intermittent basis, so can provide only an estimate of the power (W) of the flare, that is the amount of energy that the flare is producing per second.  In fire remote sensing the instantaneous measure is referred to as fire radiative power (FRP, W), while the integrated measure is fire radiative energy (FRE, J).  Sticking with that terminology, the product produces observations of gas flare FRP, and to arrive at an estimate of FRE its is needed to estimate the "uptime" of the flare.  Where "uptime" is an estimate of the proportion of time that the flare is active.

To estimate flare uptime it is needed to know the regularitiy with which a given flare is sampled (i.e. overpassed by the sensor) in cloud free conditions, as this represents an observation opportunity.  The uptime for a given period is then the total of number of times the flare is seen active over total number of observations opportunities.  If the period of observation is then assumed to be the integration period, the then FRE of a flare can be estimated as its average FRP over the period, multiplied by the total time period (in seconds) and then scaled by its uptime.  The integration period will be adjustable in the front end of the service by users, but a good default value is twelve months, giving the total amount of energy released by the flare over the twelve month period of consideration.  This can then be converted to BCM using coefficients dervied through regression analysis.    

All processes are run from service.py which manages these components.  The processing chain can be summarised as follows:
- On a daily basis all unprocessed SLSTR files from Sentinel-3A and Sentinel-3B are identified and recorded in a SQLite database.  
- The unprocessed files are batch processed to identify thermal anomalies and thier sampling using the JASMIN SLURM interface.  
- Each batch job outputs a hotspot csv file (irrespective of the presence of thermal anomalies).
- Once all batch jobs for the day are completed the service iterates over unseen hotspot csv files recording that the SLSTR file has been processed for hotspots and incorporating all hotspot data into the SQLite database.
- Given limitation of JASMIN due to security measures, direct interation with the SQLite database is not possible (e.g. through a REST API).  To pass data to the app any new persistent thermal anomalies are written to csv and made available for integration into the app using data transfer protocol (e.g. rsync)
- Once every sampling period the dataset is processed for persistent anomalies, to update the map of persistent locations.    

Current Status of service.py components:

- [x] hotspot detection 
- [x] persistent analysis
- [x] sampling detection
- [-] data transfer     
