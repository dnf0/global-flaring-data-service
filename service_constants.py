import scipy.constants as const
import queue

# ggf processing constants
DAY_NIGHT_ANGLE = 101  # degrees
SLS_SWIR_THRESHOLD = 4 * 0.06  # W m2 sr-1 um-1
T_VALUE = 0.03  # W m2 sr-1 um-1
SLS_VZA_THRESHOLD = 22  # degrees
SLS_CLOUD_WINDOW_SIZE = 33  # pixels
# Using approach from Fisher and Wooster 2019
FRP_COEFF_SWIR_16 = {'S3A': const.sigma / 8.19919059044e-09,
                     'S3B': const.sigma / 8.207028250981965e-09}
MIN_RAD = 0.1  # W m-2 sr-1 um-1
PERSISTENT_PERIOD = 30 * (24 * 60 * 60)  # seconds
COUNT_FOR_PERSISTENCE = 4
MAX_ATTEMPTS = 3  # maximum number of times a file can be sent for hotspot processing
MAX_JOB_SIZE = 1000  # max number of jobs that can be submitted to slurm in a batch
PROC_ATTEMPT_PERIOD = 7 * (24 * 60 * 60)  # seconds, only try to process an unprocessed file one a week

SAMPLING_SUFFIX = "_sampling.csv"
HOTSPOT_SUFFIX = "_hostpot.csv"
BATCH_SEPERATOR = ","
JSONL_FNAME = "activity.jsonl"

# TODO add seen.pickle filename as constant

# setup set of queues to keep track of thread processes
MAX_DB_QUEUE_SIZE = 5000
TASK_QUEUES = {
    "db": queue.PriorityQueue(maxsize=MAX_DB_QUEUE_SIZE),   # manages all database interactions
    "batch": queue.SimpleQueue(),           # manages all batch submissions
    "dump_activity": queue.SimpleQueue(),   # manages activity data dumping
    "update_unseen": queue.SimpleQueue(),   # used to instigate add_insert_unseen_files_task_to_db_queue()
    "select_batch": queue.SimpleQueue(),    # used to instigate add_select_files_for_batch_proc_task_to_db_queue()
    "update_hotspot": queue.SimpleQueue(),  # manages hotspot file process status
    "update_sample": queue.SimpleQueue(),   # manages sampling file process status
}

# db queue get priorities. Lowest valued entries are retrieved first
DUMP_ACTIVITY_PRIORITY = 1                  # Output normalised hotspot activity
DUMP_PERSISTENT_PRIORITY = 2                # Generate new persistent csv file
UPDATE_LAST_PROC_PRIORITY = 3               # Update database with last hotspot processing time
UPDATE_POPULATION_PRIORITY = 4              # Update database hotspot processing status
UPDATE_SAMPLING_UNIX_TIME_PRIORITY = 5      # Update database with last sampling processing time
UPDATE_ATTEMPT_PRIORITY = 6                 # Increment database number of hotspot processing attempts
DUMP_SEEN_PRIORITY = 7                      # Update seen.pickle from database
INSERT_UNSEEN_PRIORITY = 8                  # Update database with unseen files
INSERT_DATA_PRIORITY = 9                    # Update database with hotspot/sampling data
SELECT_FOR_BATCH_PRIORITY = 10              # Submit jobs to batch queue
