#!/home/users/dnfisher/projects/global-flaring-data-service/.venv/bin/python3
import sys
import os
import pandas as pd
import logging

import processes.utils
from processes.detector import SLSSamplingDetector, SLSHotspotDetector
import service_constants


def process_for_sampling(product: dict, sensor: str, output_path: str, output_root: str):
    sampling_detector = SLSSamplingDetector(product, sensor)
    try:
        joining_df = pd.read_csv(os.path.join(output_root, "persistent.csv"), index_col=[0])
    except FileNotFoundError:
        logging.warning(f"Persistent dataframe csv file not found! Path: {output_path}/persistent.csv)")
        return pd.DataFrame()
    if joining_df.empty:
        logging.warning("""Persistent dataframe is empty.  If a new database has been created
                          this is expected and persistent dataframe will be generated at
                          completion of service_constants.PERSISTENT_PERIOD""")
        return pd.DataFrame()
    if not sampling_detector.process_pre_check(joining_df):
        return pd.DataFrame()
    sampling_detector.run_detector()
    keys = ['latitude', 'longitude', 'local_cloudiness']
    df = sampling_detector.to_dataframe(keys=keys, joining_df=joining_df, sampling=True)
    return df


def process_for_hotspots(product: dict, sensor: str) -> pd.DataFrame:
    hotspot_detector = SLSHotspotDetector(product, sensor)
    hotspot_detector.run_detector()

    keys = ['latitude', 'longitude', 'frp', 'sza', 'vza', 'swir_16', 'swir_22', 'pixel_size']
    df = hotspot_detector.to_dataframe(keys=keys)
    return df


def run(input_path: str,
        input_file: str,
        sensor: str,
        hotspot_populated: int) -> None:

    output_path = processes.utils.define_batch_process_output_path(input_path, os.environ.get("OUTPUT_ROOT"))
    output_fname_sampling = input_file.replace(".zip", service_constants.SAMPLING_SUFFIX)
    output_fname_hotspot = input_file.replace(".zip", service_constants.HOTSPOT_SUFFIX)

    # load data, if fails record empty dataframes
    # TODO if data loading fails then perhaps do not want to dump ab empty hotspot
    #  dataframe, as this may get recorded as a success (and no more attempts made).
    #  Potentially dealt with by data.retry_dataframe_loading() but needs to be checked and tested.
    try:
        product = processes.utils.unzipper(os.path.join(input_path, input_file), os.environ.get("UNZIP_ROOT"))
    except Exception as e:
        logging.warning(e)
        df = pd.DataFrame()
        if not hotspot_populated:
            output_file_names = [output_fname_sampling, output_fname_hotspot]
        else:
            output_file_names = [output_fname_sampling]

        for output_file_name in output_file_names:
            df.to_csv(os.path.join(output_path, output_file_name))
        return

    # run sampling and dump to csv
    try:
        sampling_df = process_for_sampling(product, sensor, output_path, os.environ.get("OUTPUT_ROOT"))
        sampling_df.to_csv(os.path.join(output_path, output_fname_sampling))
    except Exception as e:
        logging.warning(e)
        df = pd.DataFrame()
        df.to_csv(os.path.join(output_path, output_fname_sampling))

        # run hotspot and dump to csv
    if not hotspot_populated:
        try:
            hotspot_df = process_for_hotspots(product, sensor)
            hotspot_df.to_csv(os.path.join(output_path, output_fname_hotspot))
        except Exception as e:
            logging.warning(e)
            df = pd.DataFrame()
            df.to_csv(os.path.join(output_path, output_fname_hotspot))


def main() -> None:
    logging.basicConfig(level=logging.INFO)

    input_args = sys.argv[1]
    input_path, input_file, hotspot_populated = input_args.split(service_constants.BATCH_SEPERATOR)
    if "sentinel3a" in input_path:
        sensor = "S3A"
    elif "sentinel3b" in input_path:
        sensor = "S3B"
    else:
        logging.error("Unknown sensor type")
        return

    run(input_path, input_file, sensor, int(hotspot_populated))


if __name__ == "__main__":
    main()
